<?php namespace App\Libs\Platform\Page;

/**
 * Class for page head i.e. content to be displayed in the '<head>' tag
 */
class Head {
	private $description;	// meta description of the page
	private $keywords;	// meta keywords of the page
	private $title;	// page title
	
	/**
	 * Constructor method
	 * 
	 * @param string $title
	 * @param string $keywords
	 * @param string $description
	 */
	public function __construct($title='', $keywords='', $description='') {
		$this->description = $description;
		$this->keywords = $keywords;
		$this->title = $title;
	}
	
	/**
	 * Get method for $this->description
	 * 
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * Get method for $this->keywords
	 * 
	 * @return string
	 */
	public function getKeywords() {
		return $this->keywords;
	}
	
	/**
	 * Get method for $this->title
	 * 
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * Set method for description property
	 * 
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * Set method for keywords property
	 * 
	 * @param string $keywords
	 */
	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}
	
	/**
	 * Set method for title property
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * Method to return the properties of the class in array format
	 * 
	 * @return array
	 */
	public function toArray() {
		return [
			'description'=>$this->description,
			'keywords'=>$this->keywords,
			'title'=>$this->title
		];
	}
}
