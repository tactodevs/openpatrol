<?php namespace App\Libs\Platform;
/**
 * Class for PlatformFactory
 * This class is used mainly to determine where the request is coming from i.e.
 * something that requires Web (HTML) response or JSON (API) response
 */

use \Request;
use \Config;

class PlatformFactory {
	private $base_url;
	private $platformType = 1;
	private static $platformTypes = ['hq'=>1, 'json'=>2];
	protected $responseType = 1;
	protected static $responseTypes = [1=>'www', 2=>'api'];
	
	/**
	 * Contructor method
	 */
	public function __construct() {
		$this->base_url = Config::get('app.base_url');
		$this->processURL();
		$this->processRoutePrefix();
	}
	
	/**
	 * Method to prcess the URL
	 */
	private function processURL() {
		$subdomain = str_replace($this->base_url, '', Request::server('HTTP_HOST'));
		
		if ($subdomain == '') {
			$this->platformType = self::$platformTypes['hq'];
		}
		else if (array_search(strtolower($subdomain), array_map('strtolower', Config::get('app.static_subdomains'))) !== FALSE) {
			$this->platformType = self::$platformTypes['hq'];
		}
		else {
			echo 'Error! Check PlatformFactory - processURL()';
			exit();
		}
	}
	
	public function getPlatformType() {
		return $this->platformType;
	}
	
	public function isApi() {
		return ($this->responseType == 2)? true : false;
	}
	
	public function isWeb() {
		return ($this->responseType == 1)? true : false;
	}
	
	public function getResponseType() {
		return $this->responseType;
	}
	
	protected function processRoutePrefix() {
		if(array_search(Request::segment(1), self::$responseTypes)) {
			$this->responseType = array_search(Request::segment(1), self::$responseTypes);
		}
		else
			$this->responseType = array_search('www', self::$responseTypes);

		if($this->isApi())
			Config::set('session.driver', 'array');
	}
}
