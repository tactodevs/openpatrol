<?php
namespace App\Libs\Platform\Validator\File;

use \File as File;
use \Illuminate\Support\MessageBag as MessageBag;

class FileValidator {
	private $errors;
	protected $extension;	// allowed file extensions
	protected $fileType;	// allowed file types i.e. image (gif, jpg, jpeg, png), CSV, etc.
	private $maxFileSize;	// in bytes
	protected $messages;
	protected $mimeType;	// allowed file mime types
	
	/**
	 * Contructor method
	 * 
	 * @param string $fileType
	 * @param int $fileSize
	 * @param array $mimeType
	 */
	public function __construct($extension=array(), $fileSize=0, $fileType='', $mimeType=array()) {
		$this->errors = new MessageBag();
		$this->extension = $extension;
		$this->fileType = $fileType;
		$this->maxFileSize = $fileSize;
		$this->messages = array(
			'extension' => 'The uploaded file can have only the following extenion(s): ' . implode(', ', $this->extension),
			'fileSize' => 'The uploaded file must not be greater than ' . $this->maxFileSize . ' bytes',
			'mimeType' => 'Only ' . implode(', ', $this->extension) . ' files are accepted'
		);
		$this->mimeType = $mimeType;
	}
	
	/**
	 * Method to set an individual message
	 * 
	 * @param string $key
	 * @param string $message
	 */
	protected function setMessage($key, $message) {
		$this->messages[$key] = $message;
	}
	
	/**
	 * Method to validate a file
	 * 
	 * @param file $file
	 * @param string $key : name of the corresponding form element / database table column
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validateFile($file, $key='file') {
		if (is_null($file) || !$file->isValid()) {	// no file or invalid file
			$this->errors->add($key, 'Kindly upload a valid ' . $this->fileType . ' file');
		}
		else {
			$this->validateFileExtension($file, $key);
			$this->validateFileMimeType($file, $key);
			$this->validateFileSize($file, $key);
		}
		
		return $this->errors;
	}
	
	/**
	 * Method to validate file based on its extension
	 * 
	 * @param File $file
	 * @param string $key : name of the corresponding form element / database table column
	 */
	private function validateFileExtension($file, $key='file') {
		if (count($this->extension) > 0) {	// allowed file extensions has been set
			if (!in_array(File::extension($file->getClientOriginalName()), $this->extension)) {
				$this->errors->add($key, $this->messages['extension']);
			}
		}
	}
	
	/**
	 * Method to validate file based on its mime type
	 * 
	 * @param File $file
	 * @param string $key : name of the corresponding form element / database table column
	 */
	private function validateFileMimeType($file, $key='file') {
		if (count($this->mimeType) > 0) {	// allowed file mime type has been set
			if (!in_array($file->getMimeType(), $this->mimeType)) {
				$this->errors->add($key, $this->messages['mimeType']);
			}
		}
	}
	
	/**
	 * Method to validate file based on its size
	 * 
	 * @param File $file
	 * @param string $key : name of the corresponding form element / database table column
	 */
	private function validateFileSize($file, $key='file') {
		if ($this->maxFileSize > 0) {	// max file size has been set
			if ($file->getSize() > $this->maxFileSize) {
				if ($this->maxFileSize > 1048576) {	// if file size is greater than 1 MB
					$this->messages['fileSize'] .= ' i.e. ' . number_format((float)$this->maxFileSize/1048576, 2, '.', '') . 'MB';
				}
				else if ($this->maxFileSize > 1024) {	// if file size is greater than 1 KB
					$this->messages['fileSize'] .= ' i.e. ' . number_format((float)$this->maxFileSize/1024, 2, '.', '') . 'KB';
				}
				
				$this->errors->add($key, $this->messages['fileSize']);
			}
		}
	}
}
