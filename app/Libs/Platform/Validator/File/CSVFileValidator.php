<?php
namespace App\Libs\Platform\Validator\File;

class CSVFileValidator extends FileValidator {
	/**
	 * Contructor method
	 * 
	 * @param string $fileType
	 * @param int $fileSize
	 * @param array $mimeType
	 */
	public function __construct($fileSize=0) {
		$extension = array('csv');
		$fileType = 'CSV';
		$mimeType = array('text/plain','text/csv');
		
		parent::__construct($extension, $fileSize, $fileType, $mimeType);	// calling the parent contructor
	}
	
	/**
	 * Method to set an individual message
	 * 
	 * @param string $key
	 * @param string $message
	 */
	public function setMessage($key, $message) {
		parent::setMessage($key, $message);
	}
}
