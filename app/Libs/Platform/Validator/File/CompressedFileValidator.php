<?php
namespace App\Libs\Platform\Validator\File;

class CompressedFileValidator extends FileValidator {
	/**
	 * Contructor method
	 * 
	 * @param int $fileSize
	 */
	public function __construct($fileSize=0) {
		$extension = array('zip');
		$fileType = 'Compressed';
		$mimeType = array('application/zip');
		
		parent::__construct($extension, $fileSize, $fileType, $mimeType);	// calling the parent contructor
	}
	
	/**
	 * Method to set an individual message
	 * 
	 * @param string $key
	 * @param string $message
	 */
	public function setMessage($key, $message) {
		parent::setMessage($key, $message);
	}
}
