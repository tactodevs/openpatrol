<?php
namespace App\Libs\Platform\Storage\LawCity;

use App\Models\LawCity as LawCity;

class EloquentLawCityRepository implements LawCityRepository {
	protected $model;
	
	/**
	 * Contructor method
	 * 
	 * @param LawCity $model
	 */
	public function __construct(LawCity $model) {
		$this->model = $model;
	}
	
	/**
	 * Method to fetch all the entries
	 */
	public function all() {
		return $this->model->all();
	}
	
	/**
	 * Method to create a new entry in the table
	 * 
	 * @param array $data : array containing the new entry's data
	 */
	public function create($data) {
		$lawCity = new LawCity;
		$lawCity->create($data);
	}
	
	/**
	 * Method to delete an existing entry from the database
	 * 
	 * @param int $id : id of the entry
	 */
	public function delete($id) {
		$resource = $this->find($id);
		
		return $resource->delete();
	}
	
	/**
	 * Method to fetch and return a particular record from the table by 'id'
	 * 
	 * @param int $id : id of the entry
	 */
	public function find($id) {
		return $this->model->find($id);
	}
	
	/**
	 * Get a paginated listing
	 * 
	 * @param int $limit
	 * @param boolean $active
	 * @param array $fields
	 * @param array $filters
	 * @param string $sort
	 * @param array $with
	 * @throws DBException
	 */
	public function listing($limit=25, $active=true, $fields=array(), $filters=array(), $sort=array('name')) {
		$order = 'ASC';	// default query sorting order
		$query = $this->model->newQuery();
		
		if ($active) {
			$query->where('is_active', '=', '1');
		}
		if (!$fields) {
			$fields = array('*');
		}
		if ($filters) {
			if (isset($filters['id'])) {
				$query->where('id', '=', $filters['id']);
			}
			if (isset($filters['search'])) {
				$query->where('name', 'LIKE', '%' . $filters['search'] . '%');
			}
			
		}
		
		try {
			foreach ($sort as $val) {
				$query->orderBy($val, $order);
			}
			
			return $query->paginate($limit, $fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
	
	/**
	 * Method to update an existing entry in the database
	 * 
	 * @param int $id : id of the entry
	 * @param array $data : array containing the entry's updated data
	 * @return model_object
	 */
	public function update($id, $data) {
		$resource = $this->model->find($id);
		
		$resource->fill($data);
		$resource->save();
		
		return $resource;
	}
	
	/**
	 * Method to fetch an entry along with the respective data based on the criteria
	 * 
	 * @param int $id
	 * @param boolean $active
	 * @param string $fields
	 * @param array $with
	 * @return model_object
	 * @throws DBException
	 */
	public function view($id, $active=true, $fields=array()) {
		$query = $this->model->newQuery();
		
		if ($active) {
			$query->where('is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array('*');
		}
		
		try {
			return $query->where('id', '=', $id)->first($fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
}
