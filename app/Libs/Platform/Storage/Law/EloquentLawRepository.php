<?php
namespace App\Libs\Platform\Storage\Law;

use App\Models\Law as Law;
use App\Models\LawCity as LawCity;
use App\Models\LawKeyword as LawKeyword;

class EloquentLawRepository implements LawRepository {
	protected $model;
	protected $lawCity;
	protected $lawKeyword;
	
	/**
	 * Contructor method
	 * 
	 * @param Law $model
	 */
	public function __construct(Law $model, LawCity $lawCity, LawKeyword $lawKeyword) {
		$this->model = $model;
		$this->lawCity = $lawCity;
		$this->lawKeyword = $lawKeyword;
	}
	
	/**
	 * Method to fetch all the entries
	 */
	public function all() {
		return $this->model->all();
	}
	
	/**
	 * Method to create a new entry in the table
	 * 
	 * @param array $data : array containing the new entry's data
	 */
	public function create($data) {
		$this->model->name = $data['name'];
		$this->model->description = $data['description'];
		$this->model->category_id = $data['category_id'];
		$this->model->is_active = 1;
		$this->model->save();
		$id = $this->model->id;
		$cities = explode(',',$data['city_id']);
		foreach ($cities as $city){
			$data['city_id'] = $city;
			$data['law_id'] = $id;
			$this->lawCity->create($data);
		}
		$keywords = explode("\n",$data['keywords']);
		
		foreach ($keywords as $keyword){
			$data['keyword'] = $keyword;
			$data['law_id'] = $id;
			$this->lawKeyword->create($data);
		}
		return $this->model;
	}
	
	/**
	 * Method to delete an existing entry from the database
	 * 
	 * @param int $id : id of the entry
	 */
	public function delete($id) {
		$resource = $this->find($id);
		
		return $resource->delete();
	}
	
	/**
	 * Method to fetch and return a particular record from the table by 'id'
	 * 
	 * @param int $id : id of the entry
	 */
	public function find($id) {
		return $this->model->find($id);
	}
	
	/**
	 * Get a paginated listing
	 * 
	 * @param int $limit
	 * @param boolean $active
	 * @param array $fields
	 * @param array $filters
	 * @param string $sort
	 * @param array $with
	 * @throws DBException
	 */
	public function listing($limit=25, $active=true, $fields=[], $filters=[], $sort=['name'],$with=[]) {
		$order = 'ASC';	// default query sorting order
		$query = $this->model->newQuery();
		$tableName = $this->model->getTable();
		
		if ($active) {
			$query->where($tableName.'.is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array($tableName.'.*');
		}
		if ($filters) {
			if (isset($filters['id'])) {
				$query->where($tableName.'.id', '=', $filters['id']);
			}
			if (isset($filters['search'])) {
				$query->where($tableName.'.name', 'LIKE', '%' . $filters['search'] . '%');
				$query->join('law_keyword', 'law_keyword.law_id','=','law.id')
						->where('law_keyword.keyword','LIKE', '%' . $filters['search'] . '%');
			}
			if (isset($filters['category_id'])) {
				$query->where($tableName.'.category_id', '=', $filters['category_id']);
			}
			if (isset($filters['city_id'])) {
				$query->join('law_city', 'law_city.law_id','=','law.id')
						->where('law_city.city_id','=',$filters['city_id']);
			}
			if (isset($filters['keywords'])) {
				$query->Leftjoin('law_keyword', 'law_keyword.law_id','=','law.id');
				$query->Where($tableName.'.name', 'LIKE', '%'.$filters['keywords'].'%')
					->orWhere('law_keyword.keyword','LIKE', '%'.$filters['keywords'].'%');
			}
			
		}
		
		if($with){
			$with = $this->model->processWithSelects($with);
		}
		
		try {
			$query->with($with);
			foreach ($sort as $val) {
				$query->orderBy($val, $order)->distinct()->groupBy($tableName.'.id');
			}
			
			return $query->paginate($limit, $fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
	
	/**
	 * Method to update an existing entry in the database
	 * 
	 * @param int $id : id of the entry
	 * @param array $data : array containing the entry's updated data
	 * @return model_object
	 */
	public function update($id, $data) {
		$resource = $this->model->find($id);
		
		$resource->name = $data['name'];
		$resource->description = $data['description'];
		$resource->category_id = $data['category_id'];
		$resource->is_active = 1;
		$resource->save();
		$id = $resource->id;
		$cities = explode(',',$data['city_id']);
		foreach ($cities as $city){
			$data['city_id'] = $city;
			$data['law_id'] = $id;
			$this->lawCity->create($data);
		}
		$keywords = explode("\n",$data['keywords']);
		
		foreach ($keywords as $keyword){
			$data['keyword'] = $keyword;
			$data['law_id'] = $id;
			$this->lawKeyword->create($data);
		}
		
		return $resource;
	}
	
	/**
	 * Method to fetch an entry along with the respective data based on the criteria
	 * 
	 * @param int $id
	 * @param boolean $active
	 * @param string $fields
	 * @param array $with
	 * @return model_object
	 * @throws DBException
	 */
	public function view($id, $active=true, $fields=[], $with = []) {
		$query = $this->model->newQuery();
		
		if ($active) {
			$query->where('is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array('*');
		}
		
		if($with){
			$with = $this->model->processWithSelects($with);
		}
		
		try {
			$query->with($with);
			
			return $query->where('id', '=', $id)->first($fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
}
