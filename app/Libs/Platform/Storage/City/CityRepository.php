<?php
namespace App\Libs\Platform\Storage\City;

interface CityRepository {
	
	public function all($limit);	// method to fetch all enries
	
	public function create($input);	// method to create a new entry
	
	public function delete($id);	// method to delete an existing entry
	
	public function find($id);	// method to find an entry by id
	
	public function findCityIdByCityStateCode($city, $stateCode); // method to find city id by city name and state code
	
	public function listing($limit, $active, $fields, $filters, $sort, $with);	// method to fetch entries matching criteria
	
	public function tabNavigation($id, $state_id=0);	// method for dealing with the navigation tabs
	
	public function update($id, $input);	// method to update an existing entry
	
	public function view($id, $active, $fields, $with);	// method to get entry by id along with other criterias
}