<?php
namespace App\Libs\Platform\Storage\City;

use App\Models\City as City;

class EloquentCityRepository implements CityRepository {
	protected $model;
	
	/**
	 * Contructor method
	 * 
	 * @param City $model
	 */
	public function __construct(City $model) {
		$this->model = $model;
	}
	
	
	/**
	 * Method to fetch all the entries from the table
	 */
	public function all($limit) {
		return $this->model->where('is_active','=',1)->take($limit)->get();
	}
	
	/**
	 * Method to insert an entry into the database
	 * 
	 * @param array $data : array containing the new entry's data
	 */
	public function create($data) {
		return $this->model->create($data['city']);
	}
	
	/**
	 * Method for datatables
	 */
	public function datatables() {
		$id = \Input::get('id');
		if ($id == 0){
			$result = $this->model
			->leftjoin('loc_states', 'loc_cities.state_id', '=', 'loc_states.id')
			->select(array(
				'loc_cities.is_active as is_active',
				'loc_cities.name as name',
				'loc_states.id as state_id',
				'loc_states.name as state_name',
				'loc_cities.id'));
		} else {
			$result = $this->model
			->leftjoin('loc_states', 'loc_cities.state_id', '=', 'loc_states.id')
			->select(array(
				'loc_cities.is_active as is_active',
				'loc_cities.name as name',
				'loc_states.id as state_id',
				'loc_states.name as state_name',
				'loc_cities.id'))
			->where('loc_states.id', '=', $id);
		}
		return \Datatables::of($result)
			->add_column('Auth','@if (Auth::user()->is_super)
						{{ 1 }}
						@endif')
			->make();	
	}
	
	/**
	 * Method to delete an existing entry from the database
	 * 
	 * @param int $id : id of the entry
	 */
	public function delete($id) {
		$resource = $this->find($id);
		
		return $resource->delete();
	}
	
	/**
	 * Method to fetch and return the corresponding database entry
	 * 
	 * @param int $id : id of the entry
	 */
	public function find($id) {
		return $this->model->find($id);
	}
	
	/**
	 * Method to fetch and return a particular record from the table by 'id'
	 * 
	 * @param string $city : name of city
	 * @param string $stateCode : statecode of the city
	 * 
	 * @return int city ID
	 */
	public function findCityIdByCityStateCode($city, $stateCode) {
		$mr =  $this->model->join('loc_states','loc_states.id','=','loc_cities.state_id')->where('loc_cities.name','=',$city)->where('loc_states.statecode','=',$stateCode)->select('loc_cities.id')->first();
		
		if($mr){
			return $mr->id;
		}
		return 0;
	}
	
	/**
	 * Get a paginated listing
	 * 
	 * @param int $limit
	 * @param boolean $active
	 * @param array $fields
	 * @param array $filters
	 * @param string $sort
	 * @param array $with
	 * @throws DBException
	 */
	public function listing($limit=25, $active=true, $fields=array(), $filters=array(), $sort=array('name'), $with=array()) {
		$order = 'ASC';	// default query sorting order
		$query = $this->model->newQuery();
		$tableName = $this->model->getTable();
		
		if ($active) {
			$query->where($tableName . '.is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array($tableName . '.*');
		}
		if ($filters) {
			if (isset($filters['id'])) {
				$query->where($tableName . '.id', '=', $filters['id']);
			}
			if (isset($filters['search'])) {
				$query->where($tableName . '.name', 'LIKE', '%' . $filters['search'] . '%');
			}
			if (isset($filters['state_id'])) {
				$query->where($tableName . '.state_id', '=', $filters['state_id']);
			}
			if (isset($filters['name'])) {
				$query->where($tableName . '.name', '=',$filters['name']);
				
			}
		}
		if ($with) {
			$with = $this->model->processWithSelects($with);
		}
		
		try {
			$query->with($with);
			foreach ($sort as $val) {
				$query->orderBy($val, $order);
			}
			
			return $query->paginate($limit, $fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
	
	/**
	 * Method to setup the navigation tabs for the show/details page
	 * 
	 * @param int $id
	 * @param int $state_id
	 * @return array
	 */
	public function tabNavigation($id, $state_id=0) {
		if ($state_id) {
			return array(
				array('key' => 'state', 'name' => 'Overview', 'url' => '/state/' . $state_id),
				array('key' => 'city', 'name' => 'Cities', 'url' => '/state/' . $state_id . '/cities')
			);
		}
		else {
			return array(
				array('key' => 'city', 'name' => 'Overview', 'url' => '/city/' . $id),
				array('key' => 'zip', 'name' => 'Zips', 'url' => '/city/' . $id . '/zips')
			);
		}
	}
	
	/**
	 * Method to update an existing entry in the database
	 * 
	 * @param type $id : id of the entry
	 * @param array $data : array containint the entry's updated data
	 * @return model_object
	 */
	public function update($id, $data) {
		$resource = $this->model->find($id);
		$rules = City::$rules;
		
		if ($resource['name']===$data['city']['name'] && $resource['state_id']==$data['city']['state_id']) {
			unset($rules['name']);
			unset($data['city']['name']);
		}
		
		$resource->fill($data['city']);
		$resource->save($rules);
		
		return $resource;
	}
	
	/**
	 * Method to fetch an entry along with the respective data based on the criteria
	 * 
	 * @param int $id
	 * @param boolean $active
	 * @param string $fields
	 * @param array $with
	 * @return model_object
	 * @throws DBException
	 */
	public function view($id, $active=true, $fields=array(), $with=array()) {
		$query = $this->model->newQuery();
		$tableName = $this->model->getTable();
		
		if ($active) {
			$query->where($tableName . '.is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array($tableName . '.*');
		}
		if ($with) {
			$with = $this->model->processWithSelects($with);
			$query->with($with);
		}
		
		try {
			return $query->with($with)->where($tableName . '.id', '=', $id)->first($fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
}
