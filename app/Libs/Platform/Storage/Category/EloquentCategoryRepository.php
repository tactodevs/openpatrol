<?php
namespace App\Libs\Platform\Storage\Category;

use App\Models\Category;

class EloquentCategoryRepository implements CategoryRepository {
	protected $model;
	
	/**
	 * Contructor method
	 * 
	 * @param Category $model
	 */
	public function __construct(Category $model) {
		$this->model = $model;
	}
	
	/**
	 * Method to fetch all the entries
	 */
	public function all($limit) {
		return $this->model->where('is_active','=',1)->take($limit)->get();
	}
	
	/**
	 * Method to create a new entry in the table
	 * 
	 * @param array $data : array containing the new entry's data
	 */
	public function create($data) {
		
		
		$this->model->name = $data['name'];
		$this->model->description = $data['description'];
		$this->model->is_active = $data['is_active'];		
		$this->model->save();
	
		return $this->model;
	}
	
	/**
	 * Method to delete an existing entry from the database
	 * 
	 * @param int $id : id of the entry
	 */
	public function delete($id) {
		$resource = $this->find($id);
		
		return $resource->delete();
	}
	
	/**
	 * Method to fetch and return a particular record from the table by 'id'
	 * 
	 * @param int $id : id of the entry
	 */
	public function find($id) {
		return $this->model->find($id);
	}
	
	/**
	 * Get a paginated listing
	 * 
	 * @param int $limit
	 * @param boolean $active
	 * @param array $fields
	 * @param array $filters
	 * @param string $sort
	 * @throws DBException
	 */
	public function listing($limit=25, $active=true, $fields=array(), $filters=array(), $sort=array('name')) {
		$order = 'ASC';	// default query sorting order
		$query = $this->model->newQuery();
		
		if ($active) {
			$query->where('is_active', '=', '1');
		}
		if (!$fields) {
			$fields = array('*');
		}
		if ($filters) {
			if (isset($filters['id'])) {
				$query->where('id', '=', $filters['id']);
			}
			if (isset($filters['search'])) {
				$query->where('name', 'LIKE', '%' . $filters['search'] . '%');
			}
			if (isset($filters['updated_at'])) {
				$query->where('updated_at', '>', new \MongoDate(strtotime($filters['updated_at'])));
			}
		}
		
		try {
			foreach ($sort as $val) {
				$query->orderBy($val, $order);
			}
			
			return $query->paginate($limit, $fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
	
	/**
	 * Method to setup the navigation tabs for the show/details page
	 * @param int $id
	 * @return array
	 */
	public function tabNavigation($id) {
		return array(
			array('key' => 'category', 'name' => 'Overview', 'url' => '/category/' . $id)
		);
	}
	
	/**
	 * Method to update an existing entry in the database
	 * 
	 * @param int $id : id of the entry
	 * @param array $data : array containing the entry's updated data
	 * @return model_object
	 */
	public function update($id, $data) {
		$resource = $this->model->find($id);

		$resource->name = $data['name'];
		$resource->description = $data['description'];
		$resource->is_active = $data['is_active'];
		$resource->save();

		return $resource;
		
	}
	
	/**
	 * Method to fetch an entry along with the respective data based on the criteria
	 * 
	 * @param int $id
	 * @param boolean $active
	 * @param string $fields
	 * @return model_object
	 * @throws DBException
	 */
	public function view($id, $active=true, $fields=array()) {
		$query = $this->model->newQuery();
		
		if ($active) {
			$query->where('is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array('*');
		}
		
		try {
			return $query->where('id', '=', $id)->first($fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
}
