<?php
namespace App\Libs\Platform\Storage\Video;

use App\Models\Video as Video;
use App\Models\VideoTag as VideoTag;
class EloquentVideoRepository implements VideoRepository {
	protected $model;
	protected $videoTag;
	private $destinationPath = 'assets/videos/';
	private $thumbnailpath = '/home/sameer/Development/openpatrol/public/assets/images/video_thumbnails/';


	/**
	 * Contructor method
	 * 
	 * @param Video $model
	 */
	public function __construct(Video $model, VideoTag $videoTag) {
		$this->model = $model;
		$this->videoTag = $videoTag;
	}
	
	/**
	 * Method to fetch all the entries
	 */
	public function all() {
		return $this->model->all();
	}
	
	/**
	 * Method to create a new entry in the table
	 * 
	 * @param array $data : array containing the new entry's data
	 */
	public function create($data) {
		if (!\File::exists(public_path($this->destinationPath))) {	// directory doesn't exists
				return false;
		}
		
		$this->model->name = $data['name'];
 		$this->model->description = $data['description'];
		$this->model->category_id = $data['category_id'];
		$this->model->lat = $data['lat'];
		$this->model->lng = $data['lng'];
		$this->model->city_id = $data['city_id'];
		$this->model->is_visible = 0;
		$this->model->user_id = $data['user_id'];
		$this->model->slug = $data['slug'];
		$this->model->save();
		
		$tags = explode(',',$data['tags']);
		foreach ($tags as $tag){
			$data['tag_id'] = $tag;
			$data['video_id'] = $this->model->id;
			$this->videoTag->create($data);
		}
		
		$file = $data['src'];
		$extension = $file->getClientOriginalExtension(); // getting image extension
		$fileName = $this->model->id.'_'.$data['user_id'].'.'.$extension; // renaming image
	    $file->move($this->destinationPath,$fileName); // uploading file to given path	
		
		/* Create a video thumbnail */
		$ffmpeg = \FFMpeg\FFMpeg::create();
		$video = $ffmpeg->open('http://www.openpatrol.com/'.$this->destinationPath.''.$fileName);
		
		$video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(10))
		->save($this->thumbnailpath.''.$this->model->id.'_'.'thumbnail.jpg');
		/* Create a video thumbnail */
		
		$this->model->thumbnail = $this->model->id.'_'.'thumbnail.jpg';
		$this->model->src = $fileName;
		$this->model->save();
		
		return $this->model;
	}
	
	/**
	 * Method to delete an existing entry from the database
	 * 
	 * @param int $id : id of the entry
	 */
	public function delete($id) {
		$resource = $this->find($id);
		
		return $resource->delete();
	}
	
	/**
	 * Method to fetch and return a particular record from the table by 'id'
	 * 
	 * @param int $id : id of the entry
	 */
	public function find($id) {
		return $this->model->find($id);
	}
	
	/**
	 * Method to fetch and return a particular record from the table by 'name'
	 * 
	 * @param int $name : name of the entry
	 */
	public function getVideoByName($name){
		
		return $this->model->where('name','=',$name)->with(['videoTag'=>function($query){
			$query->with('tag');
		}])->first();
		
	}
	/**
	 * Method to fetch and return a particular record from the table by 'name'
	 * 
	 * @param int $name : name of the entry
	 */
	public function getVideoBySlug($slug){
		
		return $this->model->where('slug','=',$slug)->with(['videoTag'=>function($query){
			$query->with('tag');
		}])->first();
		
	}
	
	/**
	 * Get a paginated listing
	 * 
	 * @param int $limit
	 * @param boolean $active
	 * @param array $fields
	 * @param array $filters
	 * @param string $sort
	 * @param array $with
	 * @throws DBException
	 */
	public function listing($limit=25, $visible=true, $fields=[], $filters=[], $sort=['name'], $with) {
		$order = 'ASC';	// default query sorting order
		$query = $this->model->newQuery();
		
		if ($visible) {
			$query->where('is_visible', '=', 1);
		}
		if (!$fields) {
			$fields = ['videos.*'];
		}
		if ($filters) {
			if (isset($filters['id'])) {
				$query->where('id', '=', $filters['id']);
			}
			if (isset($filters['search'])) {
				$query->where('name', 'LIKE', '%' . $filters['search'] . '%');
			}
			if (isset($filters['category'])) {
				$query->whereIn('category_id', $filters['category']);
			}
			if (isset($filters['city'])) {
				$query->whereIn('city_id', $filters['city']);
			}
			if (isset($filters['time'])) {
				foreach ($filters['time'] as $time){
					$times = explode('-',$time);
					$month[] = $times[0];
					$year[] = $times[1];
				}
				$query->whereRaw('Month(videos.created_at) in ('.implode(',',$month).') AND Year(videos.created_at) in ('.implode(',',$year).')');
			}
			if(isset($filters['tags'])){
				$query->join('video_tag','video_tag.video_id','=','videos.id')
					->whereRaw('video_tag.tag_id in ('.implode(',',$filters['tags']).')');
			}
		}
		
		if($with){
			$with = $this->model->processWithSelects($with);
		}
		
		try {
			$query->with($with);
			
			foreach ($sort as $val) {
				$query->orderBy($val, $order)->distinct()->groupBy('videos.id');
			}
			
			return $query->paginate($limit, $fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
	
	/**
	 * Method to update an existing entry in the database
	 * 
	 * @param int $id : id of the entry
	 * @param array $data : array containing the entry's updated data
	 * @return model_object
	 */
	public function update($id, $data) {
		$resource = $this->model->find($id);
		
		$resource->fill($data);
		$resource->save();
		
		return $resource;
	}
	
	/**
	 * Method to fetch an entry along with the respective data based on the criteria
	 * 
	 * @param int $id
	 * @param boolean $active
	 * @param string $fields
	 * @param array $with
	 * @return model_object
	 * @throws DBException
	 */
	public function view($id, $visible=true, $fields=[],$with=[]) {
		$query = $this->model->newQuery();
		
		if ($visible) {
			$query->where('is_visible', '=', 1);
		}
		if (!$fields) {
			$fields = ['*'];
		}
		if ($with) {
			$with = $this->model->processWithSelects($with);
			$query->with($with);
		}
		try {
			return $query->where('id', '=', $id)->first($fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
}
