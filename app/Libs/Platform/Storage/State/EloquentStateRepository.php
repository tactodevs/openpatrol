<?php
namespace App\Libs\Platform\Storage\State;

use App\Models\State as State;

class EloquentStateRepository implements StateRepository {
	protected $model;
	
	/**
	 * Contructor method
	 * 
	 * @param State $model
	 */
	public function __construct(State $model) {
		$this->model = $model;
	}
	
	/**
	 * Method to validate access to a particular object
	 * 
	 * @param int $id
	 * @param int $corporate_id
	 * @return boolean
	 */
	public function access($id, $corporate_id) {
		return ($this->model->find($id))? true : false;
	}
	
	/**
	 * Method to fetch all the entries
	 */
	public function all() {
		return $this->model->all();
	}
	
	/**
	 * Method to create a new entry in the table
	 * 
	 * @param array $data : array containing the new entry's data
	 */
	public function create($data) {
		return $this->model->create($data['state']);
	}
	
	/**
	 * Method for datatables
	 */
	public function datatables() {
		$id = \Input::get('id');
		if ($id == 0){
			$result = $this->model
			->leftjoin('loc_countries','loc_states.country_id','=','loc_countries.id')
			->select(array(
				'loc_states.is_active as is_active',
				'loc_states.name as name',
				'loc_states.statecode as statecode',
				'loc_countries.id as country_id',
				'loc_countries.name as country_name',
				'loc_states.id'));
		} else {
			$result = $this->model
			->leftjoin('loc_countries','loc_states.country_id','=','loc_countries.id')
			->select(array(
				'loc_states.is_active as is_active',
				'loc_states.name as name',
				'loc_states.statecode as statecode',
				'loc_countries.id as country_id',
				'loc_countries.name as country_name',
				'loc_states.id'))
			->where('loc_countries.id', '=', $id);
		}
 
		return \Datatables::of($result)
			->add_column('Auth','@if (Auth::user()->is_super)
						{{ 1 }}
						@endif')
			->make();	
	}
	
	/**
	 * Method to delete an existing entry from the database
	 * 
	 * @param int $id : id of the entry
	 */
	public function delete($id) {
		$resource = $this->find($id);
		
		return $resource->delete();
	}
	
	/**
	 * Method to fetch and return a particular record from the table by 'id'
	 * 
	 * @param int $id : id of the entry
	 */
	public function find($id) {
		return $this->model->find($id);
	}
	
	/**
	 * Get a paginated listing
	 * 
	 * @param int $limit
	 * @param boolean $active
	 * @param array $fields
	 * @param array $filters
	 * @param string $sort
	 * @param array $with
	 * @throws DBException
	 */
	public function listing($limit=25, $active=true, $fields=array(), $filters=array(), $sort=array('name'), $with=array()) {
		$order = 'ASC';	// default query sorting order
		$query = $this->model->newQuery();
		$tableName = $this->model->getTable();
		
		if ($active) {
			$query->where($tableName . '.is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array($tableName . '.*');
		}
		if ($filters) {
			if (isset($filters['id'])) {
				$query->where($tableName . '.id', '=', $filters['id']);
			}
			if (isset($filters['name'])) {
				$query->where($tableName . '.name', '=', $filters['name']);
			}
			if (isset($filters['country_id'])) {
				$query->where($tableName . '.country_id', '=', $filters['country_id']);
			}
			if (isset($filters['search'])) {
				$query->where($tableName . '.name', 'LIKE', '%' . $filters['search'] . '%');
			}
		}
		if ($with) {
			$with = $this->model->processWithSelects($with);
		}
		
		try {
			$query->with($with);
			foreach ($sort as $val) {
				$query->orderBy($val, $order);
			}
			
			return $query->paginate($limit, $fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
	
	/**
	 * Method to setup the navigation tabs for the show/details page
	 * 
	 * @param int $id
	 * @param int $country_id
	 * @return array
	 */
	public function tabNavigation($id, $country_id=0) {
		if ($country_id) {
			return array(
				array('key' => 'country', 'name' => 'Overview', 'url' => '/country/' . $country_id),
				array('key' => 'state', 'name' => 'States', 'url' => '/country/' . $country_id . '/states')
			);
		}
		else {
			return array(
				array('key' => 'state', 'name' => 'Overview', 'url' => '/state/' . $id),
				array('key' => 'city', 'name' => 'Cities', 'url' => '/state/' . $id . '/cities')
			);
		}
	}
	
	/**
	 * Method to update an existing entry in the database
	 * 
	 * @param int $id : id of the entry
	 * @param array $data : array containing the entry's updated data
	 * @return model_object
	 */
	public function update($id, $data) {
		$resource = $this->model->find($id);

		$resource->fill($data['state']);
		$resource->save();
		
		return $resource;
	}
	
	/**
	 * Method to fetch an entry along with the respective data based on the criteria
	 * 
	 * @param int $id
	 * @param boolean $active
	 * @param string $fields
	 * @param array $with
	 * @return model_object
	 * @throws DBException
	 */
	public function view($id, $active=true, $fields=array(), $with=array()) {
		$query = $this->model->newQuery();
		$tableName = $this->model->getTable();
		
		if ($active) {
			$query->where($tableName . '.is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array($tableName . '.*');
		}
		if ($with) {
			$with = $this->model->processWithSelects($with);
			$query->with($with);
		}
		
		try {
			return $query->with($with)->where($tableName . '.id', '=', $id)->first($fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
}
