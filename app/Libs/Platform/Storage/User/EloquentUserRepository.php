<?php
namespace App\Libs\Platform\Storage\User;

use App\Models\User as User;

class EloquentUserRepository implements UserRepository {
	protected $model;
	
	/**
	 * Contructor method
	 * 
	 * @param User $model
	 */
	public function __construct(User $model) {
		$this->model = $model;
	}
	
	
	/**
	 * Method to fetch all the entries from the table
	 */
	public function all($limit) {
		return $this->model->where('is_active','=',1)->take($limit)->get();
	}
	
	/**
	 * Method to insert an entry into the database
	 * 
	 * @param array $data : array containing the new entry's data
	 */
	public function create($data) {
		return $this->model->create($data['city']);
	}
	
	
	/**
	 * Method to delete an existing entry from the database
	 * 
	 * @param int $id : id of the entry
	 */
	public function delete($id) {
		$resource = $this->find($id);
		
		return $resource->delete();
	}
	
	/**
	 * Method to fetch and return the corresponding database entry
	 * 
	 * @param int $id : id of the entry
	 */
	public function find($id) {
		return $this->model->find($id);
	}

	
	/**
	 * Get a paginated listing
	 * 
	 * @param int $limit
	 * @param boolean $active
	 * @param array $fields
	 * @param array $filters
	 * @param string $sort
	 * @param array $with
	 * @throws DBException
	 */
	public function listing($limit=25, $active=true, $fields=[], $filters=[], $sort=['id'], $with=[]) {
		$order = 'ASC';	// default query sorting order
		$query = $this->model->newQuery();
		$tableName = $this->model->getTable();
		
		if ($active) {
			$query->where($tableName . '.is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array($tableName . '.*');
		}
		if ($filters) {
			if (isset($filters['id'])) {
				$query->where($tableName . '.id', '=', $filters['id']);
			}
			if (isset($filters['search'])) {
				$query->where($tableName . '.name', 'LIKE', '%' . $filters['search'] . '%');
			}
			if (isset($filters['state_id'])) {
				$query->where($tableName . '.state_id', '=', $filters['state_id']);
			}
			if (isset($filters['name'])) {
				$query->where($tableName . '.name', '=',$filters['name']);
				
			}
		}
		if ($with) {
			$with = $this->model->processWithSelects($with);
		}
		
		try {
			$query->with($with);
			foreach ($sort as $val) {
				$query->orderBy($val, $order);
			}
			
			return $query->paginate($limit, $fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
	
	/**
	 * Method to setup the navigation tabs for the show/details page
	 * 
	 * @param int $id
	 * @param int $state_id
	 * @return array
	 */
	public function tabNavigation($id, $state_id=0) {
		return array(
			array('key' => 'city', 'name' => 'Overview', 'url' => '/city/' . $id),
			array('key' => 'zip', 'name' => 'Zips', 'url' => '/city/' . $id . '/zips')
		);

	}
	
	/**
	 * Method to update an existing entry in the database
	 * 
	 * @param type $id : id of the entry
	 * @param array $data : array containint the entry's updated data
	 * @return model_object
	 */
	public function update($id, $data) {
		$resource = $this->model->find($id);
		$rules = User::$rules;
		
		if ($resource['name']===$data['city']['name'] && $resource['state_id']==$data['city']['state_id']) {
			unset($rules['name']);
			unset($data['city']['name']);
		}
		
		$resource->fill($data['city']);
		$resource->save($rules);
		
		return $resource;
	}
	
	/**
	 * Method to fetch an entry along with the respective data based on the criteria
	 * 
	 * @param int $id
	 * @param boolean $active
	 * @param string $fields
	 * @param array $with
	 * @return model_object
	 * @throws DBException
	 */
	public function view($id, $active=true, $fields=array(), $with=array()) {
		$query = $this->model->newQuery();
		$tableName = $this->model->getTable();
		
		if ($active) {
			$query->where($tableName . '.is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array($tableName . '.*');
		}
		if ($with) {
			$with = $this->model->processWithSelects($with);
			$query->with($with);
		}
		
		try {
			return $query->with($with)->where($tableName . '.id', '=', $id)->first($fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
}
