<?php
namespace App\Libs\Platform\Storage\User;

interface UserRepository {
	
	public function all($limit);	// method to fetch all enries
	
	public function create($input);	// method to create a new entry
	
	public function delete($id);	// method to delete an existing entry
	
	public function find($id);	// method to find an entry by id
		
	public function listing($limit, $active, $fields, $filters, $sort, $with);	// method to fetch entries matching criteria
	
	public function tabNavigation($id);	// method for dealing with the navigation tabs
	
	public function update($id, $input);	// method to update an existing entry
	
	public function view($id, $active, $fields, $with);	// method to get entry by id along with other criterias
}