<?php
namespace App\Libs\Platform\Storage\BrokenLaw;

use App\Models\BrokenLaw as BrokenLaw;

class EloquentBrokenLawRepository implements BrokenLawRepository {
	protected $model;
	private $destinationPath = '/home/sameer/Development/openpatrol/public/assets/images/video_screenshots/';
	/**
	 * Contructor method
	 * 
	 * @param BrokenLaw $model
	 */
	public function __construct(BrokenLaw $model) {
		$this->model = $model;
	}
	
	/**
	 * Method to fetch all the entries
	 */
	public function all() {
		return $this->model->all();
	}
	
	/**
	 * Method to create a new entry in the table
	 * 
	 * @param array $data : array containing the new entry's data
	 */
	public function create($data) {
		
		/* Capture Video Screen Shot */
		$ffmpeg = \FFMpeg\FFMpeg::create();
		$video = $ffmpeg->open('http://www.openpatrol.com/assets/videos/'.$data['file_name']);
		
		$video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds($data['time']))
		->save($this->destinationPath.''.$data['video_id'].'_'.$data['time'].'_screenshot.jpg');
		/* Capture Video Screen Shot */
		
		
		$this->model->law_id = $data['law_id'];
		$this->model->video_id = $data['video_id'];
		$this->model->time = $data['time'];
		$this->model->screen_capture_src = $data['video_id'].'_'.$data['time'].'_screenshot.jpg';
		$this->model->is_active = 1;
		$this->model->save();
		
		return $this->model;
	}
	
	/**
	 * Method to delete an existing entry from the database
	 * 
	 * @param int $id : id of the entry
	 */
	public function delete($id) {
		$resource = $this->find($id);
		
		return $resource->delete();
	}
	
	/**
	 * Method to fetch and return a particular record from the table by 'id'
	 * 
	 * @param int $id : id of the entry
	 */
	public function find($id) {
		return $this->model->find($id);
	}
	
	/**
	 * Get a paginated listing
	 * 
	 * @param int $limit
	 * @param boolean $active
	 * @param array $fields
	 * @param array $filters
	 * @param string $sort
	 * @param array $with
	 * @throws DBException
	 */
	public function listing($limit=25, $active=true, $fields=[], $filters=[], $sort=['name'],$with=[]) {
		$order = 'ASC';	// default query sorting order
		$query = $this->model->newQuery();
		$tableName = $this->model->getTable();
		
		if ($active) {
			$query->where($tableName.'.is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array($tableName.'.*');
		}
		if ($filters) {
			if (isset($filters['id'])) {
				$query->where($tableName.'.video_id', '=', $filters['id']);
			}
			if (isset($filters['search'])) {
				$query->where($tableName.'.name', 'LIKE', '%' . $filters['search'] . '%');
			}
			if (isset($filters['category_id'])) {
				$query->where($tableName.'.category_id', '=', $filters['category_id']);
			}
			if (isset($filters['city_id'])) {
				$query->join('law_city', 'law_city.law_id','=','law.id')
						->where('law_city.city_id','=',$filters['city_id']);
			}
			
		}
		if ($with) {
			$with = $this->processWithSelects($with);
		}
		
		try {
			$query->with($with);
			foreach ($sort as $val) {
				$query->orderBy($val, $order);
			}
			
			return $query->paginate($limit, $fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
	
	
	/**
	 * Method to process the 'selects' (fields) associated with the 'with'
	 * 
	 * @param type $relations
	 * @return type
	 */
	public function processWithSelects($relations) {
		if ($this->is_assoc($relations)) {
			foreach ($relations as $key=>$value) {
				if (is_array($value)) {
					$relations[$key] = function($query) use ($value) {
						return $query->select($value);
					};
				}
			}
		}
		
		return $relations;
	}
	
	
	/**
	 * Method to ensure that the array contains key-value pair where key is
	 * of type 'string'
	 * 
	 * @param array $array
	 * @return boolean
	 */
	private function is_assoc($array) {
		return (bool)count(array_filter(array_keys($array), 'is_string'));
	}
	
	
	
	/**
	 * Method to update an existing entry in the database
	 * 
	 * @param int $id : id of the entry
	 * @param array $data : array containing the entry's updated data
	 * @return model_object
	 */
	public function update($id, $data) {
		$resource = $this->model->find($id);
		
		$resource->fill($data);
		$resource->save();
		
		return $resource;
	}
	
	/**
	 * Method to fetch an entry along with the respective data based on the criteria
	 * 
	 * @param int $id
	 * @param boolean $active
	 * @param string $fields
	 * @param array $with
	 * @return model_object
	 * @throws DBException
	 */
	public function view($id, $active=true, $fields=array()) {
		$query = $this->model->newQuery();
		
		if ($active) {
			$query->where('is_active', '=', 1);
		}
		if (!$fields) {
			$fields = array('*');
		}
		
		try {
			return $query->where('id', '=', $id)->first($fields);
		}
		catch (Exception $e) {
			throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}
}
