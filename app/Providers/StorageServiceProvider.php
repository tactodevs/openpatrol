<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class StorageServiceProvider extends ServiceProvider {
	/**
	 * Method to bind services
	 */
	public function register() {	
		
		$this->app->bind(	// binding the Article Video Tag model repositories
			'App\Libs\Platform\Storage\BrokenLaw\BrokenLawRepository',
			'App\Libs\Platform\Storage\BrokenLaw\EloquentBrokenLawRepository'
		);
		
		$this->app->bind(	// binding the Article Tag model repositories
			'App\Libs\Platform\Storage\Category\CategoryRepository',
			'App\Libs\Platform\Storage\Category\EloquentCategoryRepository'
		);
		
		$this->app->bind(	// binding the Article Video Tag model repositories
			'App\Libs\Platform\Storage\City\CityRepository',
			'App\Libs\Platform\Storage\City\EloquentCityRepository'
		);
		
		
		$this->app->bind(	// binding the Article Video Tag model repositories
			'App\Libs\Platform\Storage\Country\CountryRepository',
			'App\Libs\Platform\Storage\Country\EloquentCountryRepository'
		);
		
		$this->app->bind(	// binding the Article Tag model repositories
			'App\Libs\Platform\Storage\Law\LawRepository',
			'App\Libs\Platform\Storage\Law\EloquentLawRepository'
		);
		
		$this->app->bind(	// binding the Article Video Tag model repositories
			'App\Libs\Platform\Storage\LawCity\LawCityRepository',
			'App\Libs\Platform\Storage\LawCity\EloquentLawCityRepository'
		);
		
		$this->app->bind(	// binding the Article Video Tag model repositories
			'App\Libs\Platform\Storage\LawKeyword\LawKeywordRepository',
			'App\Libs\Platform\Storage\LawKeyword\EloquentLawKeywordRepository'
		);
		
		$this->app->bind(	// binding the Article Video Tag model repositories
			'App\Libs\Platform\Storage\State\StateRepository',
			'App\Libs\Platform\Storage\State\EloquentStateRepository'
		);
		
		$this->app->bind(	// binding the Article Tag model repositories
			'App\Libs\Platform\Storage\Tag\TagRepository',
			'App\Libs\Platform\Storage\Tag\EloquentTagRepository'
		);
		
		$this->app->bind(	// binding the Article Tag model repositories
			'App\Libs\Platform\Storage\Video\VideoRepository',
			'App\Libs\Platform\Storage\Video\EloquentVideoRepository'
		);
		
		$this->app->bind(	// binding the Article Video Tag model repositories
			'App\Libs\Platform\Storage\VideoTag\VideoTagRepository',
			'App\Libs\Platform\Storage\VideoTag\EloquentVideoTagRepository'
		);
		
		$this->app->bind(	// binding the Article Video Tag model repositories
			'App\Libs\Platform\Storage\User\UserRepository',
			'App\Libs\Platform\Storage\User\EloquentUserRepository'
		);
		
	}
}
