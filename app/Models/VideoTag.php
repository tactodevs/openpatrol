<?php namespace App\Models;

class VideoTag extends \App\Models\PlatformBaseModel {
	
//	protected $dates = ['deleted_at'];	// for soft deletes
	protected $fillable = ['video_id', 'tag_id'];	// fields that can be mass assigned
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $table = 'video_tag';
	
//	protected $softDelete = true;
	
	
	public function tag(){
		return $this->hasOne('App\Models\Tag','id','tag_id');
	}
	
	/* Other Methods */
	/**
	 * Method to return the table name
	 * @return string
	 */
	public function getTable() {
		return $this->table;
	}
	/* Other Methods */
	
}