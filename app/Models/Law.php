<?php namespace App\Models;

class Law extends \App\Models\PlatformBaseModel {
	
//	protected $dates = ['deleted_at'];	// for soft deletes
	protected $fillable = ['name', 'description', 'category_id', 'is_active'];	// fields that can be mass assigned
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $table = 'law';
//	protected $softDelete = true;
	
	
	public function keyword(){
		return $this->hasMany('App\Models\LawKeyword','law_id','id');
	}
	
	public function LawCity(){
		return $this->hasMany('App\Models\LawCity','law_id','id');
	}
	
	public function category(){
		return $this->hasOne('App\Models\Category','id','category_id');
	}
	
	/* Other Methods */
	/**
	 * Method to return the table name
	 * @return string
	 */
	public function getTable() {
		return $this->table;
	}
	/* Other Methods */
	
}