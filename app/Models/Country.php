<?php namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends \App\Models\PlatformBaseModel {
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];	// for soft deletes
	protected $fillable = array('name', 'is_active');	// fields that can be mass assigned
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $softDelete = true;
	protected $table = 'loc_countries';
	
	/* Relationship Methods */
	public function states() {
		return $this->hasMany('App\Models\State');
	}
	/* Relationship Methods */
	
	/* Other Methods */
	/**
	 * Method to return the table name
	 * @return string
	 */
	public function getTable() {
		return $this->table;
	}
	/* Other Methods */
}
