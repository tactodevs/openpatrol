<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends \App\Models\PlatformBaseModel {
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];	// for soft deletes
	protected $fillable = ['name', 'src', 'lat', 'lng', 'city_id','description', 'category_id', 'is_visible', 'user_id', 'flags','slug'];	// fields that can be mass assigned
	protected $hidden = ['updated_at', 'deleted_at'];	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $table = 'videos';
	protected $softDelete = true;
	
	
	
	/* Relationship Models */
	public function videoTag(){
		return $this->hasMany('App\Models\VideoTag');
	}
	
	public function category(){
		return $this->hasOne('App\Models\Category','id','category_id');
	}
	
	public function city(){
		return $this->hasOne('App\Models\City','id','city_id');
	}
	/* Relationship Models */


	/* Other Methods */
	/**
	 * Method to return the table name
	 * @return string
	 */
	public function getTable() {
		return $this->table;
	}
	/* Other Methods */
	
}