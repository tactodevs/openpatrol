<?php namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class LawCity extends \App\Models\PlatformBaseModel {
	use SoftDeletes;

	protected $dates = ['deleted_at'];	// for soft deletes
	protected $fillable = ['law_id', 'city_id'];	// fields that can be mass assigned
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $table = 'law_city';
	
	protected $softDelete = true;
	
	
	public function law(){
		return $this->hasOne('App\Models\Law');
	}
	
	public function City(){
		return $this->hasOne('App\Models\City','id','city_id');
	}
	
	/* Other Methods */
	/**
	 * Method to return the table name
	 * @return string
	 */
	public function getTable() {
		return $this->table;
	}
	/* Other Methods */
	
}