<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends \App\Models\PlatformBaseModel {
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];    // for soft deletes
	protected $guarded = ['id', 'parent_id', 'lft', 'rgt', 'depth'];
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $softDelete = true;
	protected $table = 'categories';
	

}
