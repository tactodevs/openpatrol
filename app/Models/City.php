<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class City extends \App\Models\PlatformBaseModel {
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];	// for soft deletes
	protected $fillable = array('name', 'state_id', 'is_active');	// fields that can be mass assigned
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $table = 'loc_cities';
	protected $softDelete = true;
	
	
	/* Relationship Methods */
	public function state() {
		return $this->belongsTo('App\Models\State');
	}
	/* Relationship Methods */
	
	/* Other Methods */
	/**
	 * Method to return the table name
	 * @return string
	 */
	public function getTable() {
		return $this->table;
	}
	/* Other Methods */
}
