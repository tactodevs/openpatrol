<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class State extends \App\Models\PlatformBaseModel {
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];	// for soft deletes
	protected $fillable = array('name', 'country_id', 'is_active');	// fields that can be mass assigned
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $softDelete = true;
	protected $table = 'loc_states';
	

	
	/* Relationship Methods */
	public function cities() {
		return $this->hasMany('App\Models\City');
	}
	
	public function country() {
		return $this->belongsTo('App\Models\Country');
	}
	/* Relationship Methods */
	
	/* Other Methods */
	/**
	 * Method to return the table name
	 * @return string
	 */
	public function getTable() {
		return $this->table;
	}
	/* Other Methods */
}
