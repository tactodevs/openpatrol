<?php namespace App\Models;

use \Image as Image;
use \Eloquent;
use Illuminate\Support\Facades\Schema;

class PlatformBaseModel extends Eloquent {
	/**
	 * Contructor method
	 * 
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array()) {
		parent::__construct($attributes);
	}
	
	/**
	 * Boot method
	 */
	public static function boot() {
		parent::boot();
		
		static::creating(function($model) {
			if (Schema::hasColumn($model->getTable(), 'created_at'))
				$model->created_at = date('Y-m-d H:i:s');
			
			if(Schema::hasColumn($model->getTable(), 'created_by') && !$model->created_by)
				$model->created_by = (\Auth::check()) ? \Auth::user()->id : 0;
		});
		
		static::saving(function($model) {
			if(Schema::hasColumn($model->getTable(), 'updated_by') && !$model->updated_by)
				$model->updated_by = (Auth::check()) ? Auth::user()->id : 0;
		});
	}
	
	/**
	 * Method to ensure that the array contains key-value pair where key is
	 * of type 'string'
	 * 
	 * @param array $array
	 * @return boolean
	 */
	private function is_assoc($array) {
		return (bool)count(array_filter(array_keys($array), 'is_string'));
	}
	
	/**
	 * Method to process the 'selects' (fields) associated with the 'with'
	 * 
	 * @param type $relations
	 * @return type
	 */
	public function processWithSelects($relations) {
		if ($this->is_assoc($relations)) {
			foreach ($relations as $key=>$value) {
				if (is_array($value)) {
					$relations[$key] = function($query) use ($value) {
						return $query->select($value);
					};
				}
			}
		}
		
		return $relations;
	}
	
	/**
	 * Method to resize the image based on maximum width and height
	 * 
	 * @param image_file $image
	 * @param int $maxWidth
	 * @param int $maxHeight
	 * @return Image
	 */
	public function resizeImage($image, $maxWidth, $maxHeight) {
		$img = Image::make($image);
		
		if ($img->width() > $maxWidth) {	// resize the image if its width exceeds the allowed limit
			$img->resize($maxWidth, null, function ($constraint) { $constraint->aspectRatio(); });	// resize the image maintaining the aspect ratio
		}
		
		if ($img->height() > $maxHeight) {	// resize the image if its height exceeds the allowed limit
			$img->resize(null, $maxHeight, function ($constraint) { $constraint->aspectRatio(); });	// resize the image maintaining the aspect ratio
		}
		
		return $img;
	}
}