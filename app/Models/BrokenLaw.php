<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class BrokenLaw extends \App\Models\PlatformBaseModel {
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];	// for soft deletes
	protected $fillable = ['law_id','video_id','time','screen_capture_src','is_active','created_by'];	// fields that can be mass assigned
	protected $hidden = ['updated_at', 'deleted_at'];	//	array of fields that are to be ignored i.e. not pulled from the database
	protected $table = 'broken_law';
	protected $softDelete = true;
	
	
	
	
	public function law(){
		return $this->hasOne('App\Models\Law','id','law_id');
	}
	/* Other Methods */
	/**
	 * Method to return the table name
	 * @return string
	 */
	public function getTable() {
		return $this->table;
	}
	/* Other Methods */
	
}