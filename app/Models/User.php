<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
	protected $table = 'users';
	protected $fillable = ['firstname', 'lastname', 'email', 'password', 'dob', 'mobile', 'is_active'];
	protected $hidden = ['password', 'remember_token','created_at','updated_at','deleted_at'];

}
