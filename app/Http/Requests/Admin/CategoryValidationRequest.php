<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class CategoryValidationRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(){
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(){
		return [
			'is_active' => 'Required|Boolean',
			'name' => 'Required|Regex:/^[a-zA-Z\s]+$/i|Unique:categories,name,'.Request::segment(2).',id|Max:100',
			'description' => 'Required'
		];
	}
	
	public function messages() {
		return [
			'is_active' => 'Category must be set to either active or in-active',
			'name.max' => 'Name cannot exceed 100 characters',
			'name.regex' => 'Allowed characters for name are a-z (lowercase and capital) and space ( )',
			'name.required' => 'Name is required',
			'name.unique' => 'Name must be unique',
			'description.required' => 'Description is required'
		];
	}
}
