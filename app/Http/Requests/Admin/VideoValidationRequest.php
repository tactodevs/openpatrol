<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class VideoValidationRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(){
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(){
		return [
			'name' => 'Required|Regex:/^[a-zA-Z\s]+$/i|Max:100',
			'src' => 'Required',
			'description' => '',
			'category_id' => 'Exists:categories,id',
			'flags' => 'Integer'
		];
	}
	
	public function messages() {
		return [
			'is_visible' => 'Video must be set to either visble or in-visible',
			'name.max' => 'Name cannot exceed 100 characters',
			'name.regex' => 'Allowed characters for name are a-z (lowercase and capital) and space ( )',
			'name.required' => 'Name is required',
			'category_id.exists' => 'Please select a valid category',
			'flags.integer' => 'Flags must be an integer value',
			'src.required' => 'Video is required',
			'src.mimes' => 'Please select a proper video'
		];
	}
}
