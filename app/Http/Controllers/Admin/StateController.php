<?php namespace App\Http\Controllers\Admin;

use \App as App;
use \Input as Input;
use \Lang as Lang;
use \App\Libs\Platform\Storage\State\StateRepository as StateRepository;
use \Redirect as Redirect;
use \Response as Response;
use \View as View;
use \URL as URL;

class StateController extends AdminController {
	private $state;
	
	/**
	 * Constructor Method - inject the State Repository
	 * 
	 * @param \App\Libs\Platform\Storage\State\StateRepository $state
	 */
	public function __construct(StateRepository $state) {
		parent::__construct();
		
		$this->state = $state;
		$this->page->setActivePage('state');
		$this->page->setActiveSection(array('geo', 'state'));
	}
	
	/**
	 * Display a listing of the resource.
	 * In case of API call it will return JSON otherswise display a listing page
	 * 
	 * @param int $country_id
	 * @return Response
	 */
	public function index($country_id=0) {
		/* Default Variables */
		$active = 0;	// fetch only active entries or all entries
		$country = ($country_id)? App::make('App\Libs\Platform\Storage\Country\CountryRepository')->find($country_id) : null;
		$fields = [];	// list of fields to be fetched
		$filters = [];	// list of filters (where clause)
		$limit = 25;	// number of entries
		$metaDesc = 'List of states';	// meta description
		$metaKeywords = 'list, states, list of states, state list';	// meta keywords
		$sort = array('name');	// sorting of data
		$title = 'States List';
		$with = array(
			'Country' => array('id', 'name')
		);
		/* Default Variables */
		
		/* Input Variables */
		if (Input::has('active')) {
			$active = 1;
		}
		if (Input::has('country_id')) {
			$filters['country_id'] = Input::get('country_id');
		}
		if (Input::has('fields')) {	// separator is comma
			$fields = explode(',', Input::get('fields'));
		}
		if (Input::has('limit')) {
			$limit = Input::get('limit');
		}
		if (Input::has('search')) {
			$filters['search'] = Input::get('search');
		}
		if (Input::has('sort')) {	// separator is comma
			$sort = explode(',', Input::get('sort'));
		}
		if (Input::has('with')) {	// separator is comma
			$with = explode(',', Input::get('with'));
		}
		/* Input Variables */
		
		/* Address Dependant Tasks */
		if ($country_id) {
			$filters['country_id'] = $country_id;
			
			/* Tab Navigation */
			$tabNav = App::make('App\Libs\Platform\Storage\State\StateRepository')->tabNavigation(0, $country_id);
			$this->page->getBody()->addToData('tabNav', $tabNav);
			/* Tab Navigation */
			
			/* Enhance Meta Information and Title */
			$metaDesc = 'List of ' . addslashes($country['name']) . ' states';
			$metaKeywords = 'list, states, ' . addslashes($country['name']) . ' states, list of ' . addslashes($country['name']) . ' states, ' . addslashes($country['name']) . ' state list';
			$title = $country['name'] . ': ' . $title;
			/* Enhance Meta Information and Title */
		}
		/* Address Dependant Tasks */
		
		/* Get Data for View */
		$response = $this->state->listing($limit, $active, $fields, $filters, $sort, $with);
		/* Get Data for View */
		
		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Breadcrumbs */
		if ($country_id) {
			$this->page->getBody()->addBreadcrumb('Country', 'country', 'admin.country.index');
			$this->page->getBody()->addBreadcrumb($country['name'], 'country/' . $country_id, 'admin.country.index');
			$this->page->getBody()->addBreadcrumb('States', 'country/' . $country_id . '/states', 'admin.state.index');
		}
		else {
			$this->page->getBody()->addBreadcrumb('State', 'state', 'admin.state.index');
		}
		$this->page->getBody()->addBreadcrumb('Listing');
		/* Breadcrumbs */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('list', $response);
		$this->page->getBody()->addToData('country', $country);
		/* Set Data for View */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Show the form for creating a new resource.
	 * 
	 * @return Response
	 */
	public function create() {
		/* Default Variables */
		$country = null;
		$country_id = '';
		$metaDesc = 'Create a new state';	// meta description
		$metaKeywords = 'create, new, state, new state, create state, create new state';	// meta keywords
		$title = 'Create State';
		/* Default Variables */
		
		/* Input Variables */
		if (Input::has('country_id')) {
			$country_id = Input::get('country_id');
		}
		/* Input Variables */
		
		/* Set Dependencies */
		if ($country_id) {
			try {
				$country = App::make('App\Libs\Platform\Storage\Country\CountryRepository')->find($country_id)->toArray();
			} catch (Exception $e) {
				throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
			}
		}
		/* Set Dependencies */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('State', 'state');
		$this->page->getBody()->addBreadcrumb('Create');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('country', $country);
		/* Set Data for View */
		
		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Store a newly created resource in storage.
	 * 
	 * @return Response
	 */
	public function store() {
		/* Separation & Limitations of Data By Models */
		$data['state'] = Input::only('name', 'statecode', 'country_id', 'is_active');
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->state->create($data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		if (count($mr->errors()) === 0) { // If Successful
			return Redirect::to('state/' . $mr->id)->with(array('success' => Lang::get('messages.crud.create.success', array('action' => 'created'))));
		}
		else { // If Error
			return Redirect::to(URL::previous())->withInput()->withErrors($mr->errors());
		}
		/* Redirect Based on Model Response */
	}
	
	/**
	 * Display the specified resource.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function show($id) {
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Details of state: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'State: ';
		$with = array(
			'Country' => array('id', 'name')
		);
		/* Default Variables */
		
		/* Specific Variables for API Request */
		if ($this->platform->isApi()) {
			$with = array(
				'creator' => array('id', 'firstname', 'lastname', 'display_name')
			);
		}
		/* Specific Variables for API Request */
		
		/* Get Country */
		$response = $this->state->view($id, $active, $fields, $with);
		/* Get Country */
		
		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'details, state details, ' . addslashes($response['name']) . ', ' . addslashes($response['name']) . ' details';
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('state', $response);
		/* Set Data for View */
		
		/* Tab Navigation */
		$tabNav = App::make('App\Libs\Platform\Storage\State\StateRepository')->tabNavigation($id);
		$this->page->getBody()->addToData('tabNav', $tabNav);
		/* Tab Navigation */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('State', 'state');
		$this->page->getBody()->addBreadcrumb($response['name']);
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Show the form for editing the specified resource.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function edit($id) {
		
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Edit details of state: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'Edit State: ';
		$with = array(
			'Country' => array('id', 'name')
		);
		/* Default Variables */
		
		/* Get Country */
		$response = $this->state->view($id, $active, $fields, $with);
		/* Get Country */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'manage, edit, manage state, edit state, edit ' . addslashes($response['name']) . ', manage '. addslashes($response['name']);
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('State', 'state');
		$this->page->getBody()->addBreadcrumb($response['name'], 'state/' . $response['id']);
		$this->page->getBody()->addBreadcrumb('Edit');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* Set Data To View */
		$this->page->getBody()->addToData('state', $response);
		/* Set Data To View */

		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Update the specified resource in storage.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function update($id) {
		/* Separation & Limitations of Data By Models */
		$data['state'] = Input::only('name', 'statecode', 'country_id', 'is_active');
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->state->update($id, $data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		if (count($mr->errors()) === 0) { // If Successful
			return Redirect::to('state/' . $id)->with(array('success' => Lang::get('messages.crud.update.success', array('action' => 'updated'))));
		}
		else {
			return Redirect::to('state/' . $id . '/edit')->withInput()->withErrors($mr->errors());
		}
		/* Redirect Based on Model Response */
	}
	
	/**
	 * Remove the specified resource from storage.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function destroy($id) {
		/* Access Check */
		$this->access($this->viewBase . '.' . __FUNCTION__, App::make('App\Libs\Platform\Storage\State\StateRepository'), $id);
		/* Access Check */
		
		/* Query Creation & Fire */
		$this->state->delete($id);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		if (Input::has('country_id')) {
			return Redirect::to(URL::to('country/' . Input::get('country_id') . '/states'))->with(array('success' => Lang::get('messages.crud.delete.success', array('action' => 'deleted'))));
		}
		else {
			return Redirect::to(URL::to('state'))->with(array('success' => Lang::get('messages.crud.delete.success', array('action' => 'deleted'))));
		}
		
		/* Redirect Based on Model Response */
	}
	
	/* Datatables */
	public function datatables(){
		
		return $this->state->datatables();
	}
	/* Datatables */
}
