<?php namespace App\Http\Controllers\Admin;

use App\Libs\Platform\Storage\User\UserRepository;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Admin\UserValidationRequest;

class UserController extends AdminController {
	private $user;
	
	/**
	 * Constructor method - inject the User Repository
	 * 
	 * @param App\Libs\Platform\Storage\User\UserRepository $user
	 */
	public function __construct(UserRepository $user) {
		parent::__construct();
		
		$this->user = $user;
		$this->page->setActivePage('user');
		$this->page->setActiveSection(['user']);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		/* Default Variables */
		$active = 0;	// fetch only active entries or all entries
		$fields = [];	// list of fields to be fetched
		$filters = [];	// list of filters (where clause)
		$limit = 25;	// number of entries
		$metaDesc = 'List of User';	// meta description
		$metaKeywords = 'list, user, list of user, user list';	// meta keywords
		$sort = ['id'];	// sorting of data
		$title = 'User List';
		$with = [];
		/* Default Variables */
		
		/* Input Variables */
		if (Input::has('active')) {
			$active = 1;
		}
		if (Input::has('fields')) {	// separator is comma
			$fields = explode(',', Input::get('fields'));
		}
		if (Input::has('limit')) {
			$limit = Input::get('limit');
		}
		if (Input::has('search')) {
			$filters['search'] = Input::get('search');
		}
		if (Input::has('sort')) {	// separator is comma
			$sort = explode(',', Input::get('sort'));
		}
		/* Input Variables */
		
		/* Get Data */
		$response = $this->user->listing($limit, $active, $fields, $filters, $sort, $with);
		/* Get Data */
		
		/* JSON Response for API */
		if ($this->platform->isApi()) {
			return response($response->toArray(), 200);
		}
		/* JSON Response for API */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('User', 'user', 'admin.user.index');
		$this->page->getBody()->addBreadcrumb('Listing');
		/* Breadcrumbs */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('list', $response);
		/* Set Data for View */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		/* Default Variables */
		$metaDesc = 'Create a new user';	// meta description
		$metaKeywords = 'create, new, user, new user, create user, create new user';	// meta keywords
		$title = 'Create User';
		/* Default Variables */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('User', 'user');
		$this->page->getBody()->addBreadcrumb('Create');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserValidationRequest $userValidator){
		/* Separation & Limitations of Data By Models */
		$data = Input::only('name', 'is_active');
		$data['created_by'] = '1_Sameer';
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->user->create($data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('user/' . $mr->id);
		/* Redirect Based on Model Response */
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Details of user: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'User: ';
		/* Default Variables */
		
		/* Get User */
		$response = $this->user->view($id, $active, $fields);
		/* Get User */

		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'details, user details, ' . addslashes($response['firstname']) . ', ' . addslashes($response['lastname']) . ' details';
		$title .= addslashes($response['firstname']);
		/* Enhance Meta Information and Title */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('user', $response);
		/* Set Data for View */
		
		/* Tab Navigation */
//		$tabNav = App::make('Platform\Storage\User\UserRepository')->tabNavigation($id);
//		$this->page->getBody()->addToData('tabNav', $tabNav);
		/* Tab Navigation */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('User', '/user');
		$this->page->getBody()->addBreadcrumb($response['firstname'], '/user/' . $response['id'], 'admin.user.show');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Edit details of user: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'Edit User: ';
		$with = [];
		/* Default Variables */
		
		/* Get User */
		$response = $this->user->view($id, $active, $fields, $with);
		/* Get User */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['firstname']);
		$metaKeywords .= 'manage, edit, manage user, edit user, edit ' . addslashes($response['name']) . ', manage '. addslashes($response['name']);
		$title .= addslashes($response['firstname']);
		/* Enhance Meta Information and Title */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('User', '/user');
		$this->page->getBody()->addBreadcrumb($response['firstname'], '/user/' . $response['id']);
		$this->page->getBody()->addBreadcrumb('Edit');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* Set Data To View */
		$this->page->getBody()->addToData('user', $response);
		/* Set Data To View */

		/* HTML View Response */
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,UserValidationRequest $userValidator){
		/* Separation & Limitations of Data By Models */
		$data = Input::only('name', 'is_active');
		$data['created_by'] = '1_Sameer';
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->user->update($id,$data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('user/' . $mr->_id);
		/* Redirect Based on Model Response */
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		/* Query Creation & Fire */
		$this->user->delete($id);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('user');
		/* Redirect Based on Model Response */
	}
	
}
