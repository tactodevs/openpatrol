<?php namespace App\Http\Controllers\Admin;

use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libs\Platform\Page\PageManager;
use Illuminate\Http\Request;

//use \Auth as Auth;
//use \Lang as Lang;

class AdminController extends Controller {
	protected $page = null;	// PageManager object
	protected $platform;	// type of platform i.e. api or html (web)
	protected $viewBase = '';	// base directory path of the view file
	
	/**
	 * Constructor method
	 */
	public function __construct() {
		$this->platform = App::make('pf');
		
		/* Run Necessary Middleware */
		if (!$this->platform->isApi()) {	// if platform is not API
//			$this->beforeMiddleware('csrf', array('on' => array('delete', 'post', 'put')));
		}
		/* Run Necessary Middleware */
		
		$this->page = new PageManager();
		$this->page->setActivePage(str_replace('controller', '', strtolower(substr(get_class(), strrpos(get_class(), '\\') + 1))));
		$this->page->getBody()->addBreadcrumb('Home', '/');
		$this->viewBase = str_replace('\\', '.', str_replace('controller', '', str_replace('app\\http\\controllers\\', '', strtolower(get_called_class()))));
		$this->viewBase = str_replace('controller', '', $this->viewBase);
	}
	
	public function getCreator() {
		return Auth::user()->_id . '_' . Auth::user()->name;
	}
	
	/**
	 * This function is used by different methods in controller to restrict
	 * user access by checking the passed object and object id
	 * 
	 * @param string $permission : permisson passed by function
	 * @param object $obj : object of the corresponding model
	 * @param int $obj_id : id of the corresponding model i.e. primary key
	 * @return boolean
	 */
//	protected function access($permission, $obj=null, $obj_id=0) {
//		$accessFlag = false;
//		$response = null;
//		
//		if (!Auth::user()->is_super) {	// user is not a super admin
//			/* Get Page Access */
//			$response = App::make('Platform\Storage\RolePermission\RolePermissionRepository')
//				->getByRoleIdPermission(Auth::user()->role_id, $permission, array('permission_id'));
//			/* Get Page Access */
//			
//			if (!$response) {	// user doesn't have access
//				App::abort(403, 'messages.errors.access');
//			}
//			else {	// user has access
//				if ($obj) {
//					/* Get obj */
//					$response = $obj->find($obj_id);
//					/* Get obj */
//					
//					if (!$response) {	// object doesn't exist
//						App::abort(404, Lang::get('messages.errors.missing'));
//					}
//					else {	// object exists
//						if ($obj->access($obj_id, Auth::user()->corporate_id)) {	// user can access the object
//							$accessFlag = true;
//						}
//						else {	// user can't access object
//							App::abort(403, 'messages.errors.access');
//						}
//					}
//				}
//				else {	// user requesting to view index page and has access to it
//					$accessFlag = true;
//				}
//			}
//		}
//		else {	// user has access - super user has all access
//			$accessFlag = true;
//		}
//		
//		return $accessFlag;
//	}
	
	/**
	 * Method to set the Layout
	 */
	protected function setupLayout() {
		if (!is_null($this->layout))
			$this->layout = View::make($this->layout);
	}
}
