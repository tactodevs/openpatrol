<?php namespace App\Http\Controllers\Admin;

use App\Libs\Platform\Storage\Law\LawRepository;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Admin\LawValidationRequest;
use App\Libs\Platform\Validator\File\CSVFileValidator;
use Illuminate\Support\MessageBag;
use \File as File;
use \Keboola\Csv\CsvFile as CSVReader;

class LawController extends AdminController {
	private $archiveFilePath = 'data-files/archives/bulk-upload/law/';
	private $bulkuploadPath = 'data-files/uploads/bulk-upload/law/';
	private $law;
	
	/**
	 * Constructor method - inject the Law Repository
	 * 
	 * @param App\Libs\Platform\Storage\Law\LawRepository $law
	 */
	public function __construct(LawRepository $law) {
		parent::__construct();
		
		$this->law = $law;
		$this->page->setActivePage('law');
		$this->page->setActiveSection(['law']);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		/* Default Variables */
		$active = 0;	// fetch only active entries or all entries
		$fields = [];	// list of fields to be fetched
		$filters = [];	// list of filters (where clause)
		$limit = 25;	// number of entries
		$metaDesc = 'List of Law';	// meta description
		$metaKeywords = 'list, law, list of law, law list';	// meta keywords
		$sort = ['name'];	// sorting of data
		$title = 'Law List';
		$with = ['category'];
		/* Default Variables */
		
		/* Input Variables */
		if (Input::has('active')) {
			$active = 1;
		}
		if (Input::has('fields')) {	// separator is comma
			$fields = explode(',', Input::get('fields'));
		}
		if (Input::has('limit')) {
			$limit = Input::get('limit');
		}
		if (Input::has('search')) {
			$filters['search'] = Input::get('search');
		}
		if (Input::has('sort')) {	// separator is comma
			$sort = explode(',', Input::get('sort'));
		}
		if (Input::has('category_id')) {
			$filters['category_id'] = Input::get('category_id');
		}
		if (Input::has('city_id')) {
			$filters['city_id'] = Input::get('city_id');
		}
		if (Input::has('keywords')) {
			$filters['keywords'] = Input::get('keywords');
		}
		/* Input Variables */
		
		/* Get Data */
		$response = $this->law->listing($limit, $active, $fields, $filters, $sort ,$with);
		/* Get Data */
		
		/* JSON Response for API */
		if ($this->platform->isApi()) {
			return response($response->toArray(), 200);
		}
		/* JSON Response for API */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Law', 'law', 'hq.law.index');
		$this->page->getBody()->addBreadcrumb('Listing');
		/* Breadcrumbs */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('list', $response);
		/* Set Data for View */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		/* Default Variables */
		$metaDesc = 'Create a new law';	// meta description
		$metaKeywords = 'create, new, law, new law, create law, create new law';	// meta keywords
		$title = 'Create Law';
		/* Default Variables */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Law', 'law');
		$this->page->getBody()->addBreadcrumb('Create');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(LawValidationRequest $lawValidator){
		/* Separation & Limitations of Data By Models */
		$data = Input::only('name', 'description','city_id','category_id', 'keywords', 'is_active');
		/* Separation & Limitations of Data By Models */
	
		/* Query Creation & Fire */
		$mr = $this->law->create($data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('law/' . $mr->id);
		/* Redirect Based on Model Response */
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Details of law: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'Law: ';
		$with = ['category','LawCity','keyword','LawCity.City'];
		/* Default Variables */

		/* Get Law */
		$response = $this->law->view($id, $active, $fields ,$with);
		/* Get Law */

		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'details, law details, ' . addslashes($response['name']) . ', ' . addslashes($response['name']) . ' details';
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('law', $response);
		/* Set Data for View */
		
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Law', '/law');
		$this->page->getBody()->addBreadcrumb($response['name'], '/law/' . $response['id'], 'admin.law.show');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Edit details of law: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'Edit Law: ';
		$with = ['category','lawCity.City','keyword'];
		/* Default Variables */
		
		/* Get Law */
		$response = $this->law->view($id, $active, $fields, $with);
		/* Get Law */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'manage, edit, manage law, edit law, edit ' . addslashes($response['name']) . ', manage '. addslashes($response['name']);
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Law', '/law');
		$this->page->getBody()->addBreadcrumb($response['name'], '/law/' . $response['id']);
		$this->page->getBody()->addBreadcrumb('Edit');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* Set Data To View */
		$this->page->getBody()->addToData('law', $response);
		/* Set Data To View */

		/* HTML View Response */
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,LawValidationRequest $lawValidator){
		/* Separation & Limitations of Data By Models */
		$data = Input::only('name', 'description','city_id','category_id', 'keywords', 'is_active');
		$data['created_by'] = '1_Sameer';
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->law->update($id,$data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('law/' . $mr->id);
		/* Redirect Based on Model Response */
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		/* Query Creation & Fire */
		$this->law->delete($id);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('law');
		/* Redirect Based on Model Response */
	}

	/**
	 * Display the bulk upload page
	 * 
	 * @return Response
	 */
	public function bulkUpload() {
		
		/* Default Variables */
		$metaDesc = 'Bulk upload and insert laws';	// meta description
		$metaKeywords = 'bulk, upload, insert, bulk upload, bulk insert, law, bulk upload laws, bulk insert laws';	// meta keywords
		$title = 'Law Bulk Upload';
		/* Default Variables */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Law', '/law');
		$this->page->getBody()->addBreadcrumb('Bulk Upload');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Method to process the bulk upload file
	 * 
	 * @return Response
	 */
	public function bulkUploadProcess() {
		/* Default Variables */
		$csvFileValidator = new CSVFileValidator(10485760);	// for CSV file validation - max file size is 10MB
		$dataFile = Input::hasFile('dataFile')? Input::file('dataFile') : null;
		$errors = new MessageBag();
		$is_header = Input::has('is_header')? Input::get('is_header') : 0;
		/* Default Variables */
		
		/* File Validation */
		$errors->merge($csvFileValidator->validateFile($dataFile, 'dataFile'));	// validate the CSV file
		/* File Validation */
		
		if ($errors->count() > 0) {	// some validation failed
			return Redirect::to('law/bulk-upload')->withErrors($errors);
		}
		
		/* Save CSV File */
		$newCSVFileName = 'law_' . date('YmdHis') . '.' . File::extension($dataFile->getClientOriginalName());
		$dataFile->move(public_path($this->bulkuploadPath), $newCSVFileName);
		if ($dataFile->isValid()) {	// file couldn't be saved
			$errors->add('dataFile', 'Oops! Something went wrong with the file upload. Kindly try again.');
			return redirect('law/bulk-upload')->withErrors($errors);
		}
		/* Save CSV File */
		
		/* Local Variables */
		$archiveFile = new CSVReader(public_path($this->archiveFilePath . $newCSVFileName));
		$counter = 1;	// counter
		$columnCount = 2;	// correct number of columns in the CSV file
		$csvFile = new CSVReader($this->bulkuploadPath . $newCSVFileName);	// the uploaded (and saved) CSV file
		$errorCount = 0;	// number of failed inserts
		$insertCount = 0;	// number of successful inserts
		/* Local Variables */
		
		/* File Read and Insert */
		foreach ($csvFile as $row) {
			if ($is_header && $counter==1) {	// header is set and currently read 1 line
				$archiveFile->writeRow($row);
				$counter++;
				continue;
			}
			
			if (count($row) == $columnCount) {	// file should contain the right number of columns
				$data = [];
				$data['name'] = isset($row[0]) ? $row[0] : '';
				$data['is_active'] = isset($row[1]) ? $row[1] : '';
				
				$lawValidation = new LawValidationRequest;
				$validation = \Validator::make($data,$lawValidation->rules());
				/* Query Creation & Fire */
				
				/* Redirect Based on Model Response */
				if ($validation->passes()) { // If Successful
					$mr = $this->law->create($data);
					$row[] = 'successful';	// add status to $row
					$archiveFile->writeRow($row);	// write to the archive file
					$insertCount++;
					$counter++;
				}
				else { // If Error
					$messages = $validation->messages();
					$row[] = 'fail';	// add status to $row
					$errorArray = [];
					foreach ($messages->all() as $k=>$v) {
						$errorArray[] = $k . ': ' . $v;
					}
					$row[] = implode('|', $errorArray);	// add message to $row
					
					$archiveFile->writeRow($row);	// write to the archive file
					$errorCount++;
					$counter++;
				}
				/* Redirect Based on Model Response */
			}
			else {	// file doesn't contain the right number of columns
				$row[] = 'fail';	// add status to $row
				$row[] = 'Column count must be ' . $columnCount;	// add message to $row
				$archiveFile->writeRow($row);	// write to the archive file
				$errorCount++;
				$counter++;
			}
		}
		/* File Read and Insert */
		
		/* Delete Uploaded File */
		unlink(public_path($this->bulkuploadPath . $newCSVFileName));
		/* Delete Uploaded File */
		
		return redirect('law/bulk-upload')->with(array('success'=>$insertCount . ' rows sucessfully inserted and ' . $errorCount . ' rows could not be inserted', 'file' => $this->archiveFilePath . $newCSVFileName));
	}
	
}
