<?php namespace App\Http\Controllers\Admin;

use \App as App;
use \Input as Input;
use \Lang as Lang;
use \App\Libs\Platform\Storage\City\CityRepository as CityRepository;
use \Redirect as Redirect;
use \Response as Response;
use \View as View;
use \URL as URL;

class CityController extends AdminController {
	private $city;
	
	/**
	 * Constructor method - inject the City Repository
	 * 
	 * @param \App\Libs\Platform\Storage\City\CityRepository $city
	 */
	public function __construct(CityRepository $city) {
		parent::__construct();
		
		$this->city = $city;
		$this->page->setActivePage('city');
		$this->page->setActiveSection(array('location','city'));
	}
	
	/**
	 * Display a listing of the resource.
	 * In case of API call it will return JSON otherswise display a listing page
	 * 
	 * @param int $state_id
	 * @return Response
	 */
	public function index($state_id=0) {
		
		/* Default Variables */
		$active = 0;	// fetch only active entries or all entries
		$fields = [];	// list of fields to be fetched
		$filters = [];	// list of filters (where clause)
		$limit = 25;	// number of entries
		$metaDesc = 'List of cities';	// meta description
		$metaKeywords = 'list, cities, list of cities, city list';	// meta keywords
		$sort = array('name');	// sorting of data
		$state = ($state_id)? App::make('App\Libs\Platform\Storage\State\StateRepository')->find($state_id) : null;
		$title = 'Cities List';
		$with = array(
			'State' => array('id', 'name')
		);
		/* Default Variables */
		
		/* Input Variables */
		if (Input::has('active')) {
			$active = 1;
		}
		if (Input::has('fields')) {	// separator is comma
			$fields = explode(',', Input::get('fields'));
		}
		if (Input::has('limit')) {
			$limit = Input::get('limit');
		}
		if (Input::has('search')) {
			$filters['search'] = Input::get('search');
		}
		if (Input::has('state_id')) {
			$filters['state_id'] = Input::get('state_id');
		}
		if (Input::has('sort')) {	// separator is comma
			$sort = explode(',', Input::get('sort'));
		}
		if (Input::has('with')) {	// separator is comma
			$with = explode(',', Input::get('with'));
		}
		/* Input Variables */
		
		/* Address Dependant Tasks */
		if ($state_id) {
			$filters['state_id'] = $state_id;
			
			/* Tab Navigation */
			$tabNav = App::make('App\Libs\Platform\Storage\City\CityRepository')->tabNavigation(0, $state_id);
			$this->page->getBody()->addToData('tabNav', $tabNav);
			/* Tab Navigation */
			
			/* Enhance Meta Information and Title */
			$metaDesc = 'List of ' . addslashes($state['name']) . ' cities';
			$metaKeywords = 'list, cities, ' . addslashes($state['name']) . ' cities, list of ' . addslashes($state['name']) . ' cities, ' . addslashes($state['name']) . ' city list';
			$title = $state['name'] . ': ' . $title;
			/* Enhance Meta Information and Title */
		}
		/* Address Dependant Tasks */
		
		/* Get Data for View */
		$response = $this->city->listing($limit, $active, $fields, $filters, $sort, $with);
		/* Get Data for View */
		
		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Breadcrumbs */
		if ($state_id) {
			$this->page->getBody()->addBreadcrumb('State', 'state', 'admin.state.index');
			$this->page->getBody()->addBreadcrumb($state['name'], 'state/' . $state_id, 'admin.state.index');
			$this->page->getBody()->addBreadcrumb('Cities', 'state/' . $state_id . '/cities', 'admin.city.index');
		}
		else {
			$this->page->getBody()->addBreadcrumb('City', 'city', 'admin.city.index');
		}
		$this->page->getBody()->addBreadcrumb('Listing');
		/* Breadcrumbs */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('list', $response);
		$this->page->getBody()->addToData('state', $state);
		/* Set Data for View */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Show the form for creating a new resource.
	 * 
	 * @return Response
	 */
	public function create() {
		/* Default Variables */
		$metaDesc = 'Create a new city';	// meta description
		$metaKeywords = 'create, new, city, new city, create city, create new city';	// meta keywords
		$state = null;
		$state_id = Input::old('state_id')? Input::old('state_id') : (Input::has('state_id')? Input::get('state_id') : null);
		$title = 'Create City';
		/* Default Variables */
		
		/* Set Dependencies */
		if ($state_id) {
			try {
				$state = App::make('App\Libs\Platform\Storage\State\StateRepository')->find($state_id)->toArray();
			} catch (Exception $e) {
				throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
			}
		}
		/* Set Dependencies */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('City', 'city');
		$this->page->getBody()->addBreadcrumb('Create');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('state', $state);
		/* Set Data for View */
		
		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Store a newly created resource in storage.
	 * 
	 * @return Response
	 */
	public function store() {
		/* Separation & Limitations of Data By Models */
		$data['city'] = Input::only('name', 'state_id', 'citycode', 'latitude', 'longitude', 'alias', 'is_active');
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->city->create($data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		if (count($mr->errors()) === 0) { // If Successful
			return Redirect::to('city/' . $mr->id)->with(array('success' => Lang::get('messages.crud.create.success', array('action' => 'created'))));
		}
		else { // If Error
			return Redirect::to(URL::previous())->withInput()->withErrors($mr->errors());
		}
		/* Redirect Based on Model Response */
	}
	
	/**
	 * Display the specified resource.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function show($id) {
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Details of city: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'City: ';
		$with = array(
			'State' => array('id', 'name')
		);
		/* Default Variables */
		
		/* Specific Variables for API Request */
		if ($this->platform->isApi()) {
			$with = array(
				'creator' => array('id', 'firstname', 'lastname', 'display_name')
			);
		}
		/* Specific Variables for API Request */
		
		/* Get City */
		$response = $this->city->view($id, $active, $fields, $with);
		/* Get City */
		
		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'details, city details, ' . addslashes($response['name']) . ', ' . addslashes($response['name']) . ' details';
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('city', $response);
		/* Set Data for View */
		
		/* Tab Navigation */
		$tabNav = App::make('App\Libs\Platform\Storage\City\CityRepository')->tabNavigation($id);
		$this->page->getBody()->addToData('tabNav', $tabNav);
		/* Tab Navigation */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('City', 'city');
		$this->page->getBody()->addBreadcrumb($response['name']);
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Show the form for editing the specified resource.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function edit($id) {
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Edit details of city: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$state = null;
		$state_id = Input::old('state_id')? Input::old('state_id') : '';
		$title = 'Edit City: ';
		$with = array(
			'State' => array('id', 'name')
		);
		/* Default Variables */
		
		/* Get City */
		$response = $this->city->view($id, $active, $fields, $with);
		/* Get City */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'manage, edit, manage city, edit city, edit ' . addslashes($response['name']) . ', manage '. addslashes($response['name']);
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Set Dependencies */
		if ($state_id) {
			try {
				$state = App::make('App\Libs\Platform\Storage\State\StateRepository')->find($state_id)->toArray();
			} catch (Exception $e) {
				throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
			}
		}
		/* Set Dependencies */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('City', 'city');
		$this->page->getBody()->addBreadcrumb($response['name'], 'city/' . $response['id']);
		$this->page->getBody()->addBreadcrumb('Edit');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* Set Data To View */
		$this->page->getBody()->addToData('city', $response);
		$this->page->getBody()->addToData('state', $state);
		/* Set Data To View */

		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Update the specified resource in storage.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function update($id) {
		/* Separation & Limitations of Data By Models */
		$data['city'] = Input::only('name', 'citycode', 'state_id', 'latitude', 'longitude', 'alias', 'is_active');
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->city->update($id, $data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		if (count($mr->errors()) === 0) { // If Successful
			return Redirect::to('city/' . $id)->with(array('success' => Lang::get('messages.crud.update.success', array('action' => 'updated'))));
		}
		else {
			return Redirect::to('city/' . $id . '/edit')->withInput()->withErrors($mr->errors());
		}
		/* Redirect Based on Model Response */
	}
	
	/**
	 * Remove the specified resource from storage.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function destroy($id) {
		
		/* Query Creation & Fire */
		$this->city->delete($id);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		if (Input::has('state_id')) {
			return Redirect::to(URL::to('state/' . Input::get('state_id') . '/cities'))->with(array('success' => Lang::get('messages.crud.delete.success', array('action' => 'deleted'))));
		}
		else {
			return Redirect::to(URL::to('city'))->with(array('success' => Lang::get('messages.crud.delete.success', array('action' => 'deleted'))));
		}
		/* Redirect Based on Model Response */
	}
		
	/* Datatables */
	public function datatables(){
		
		return $this->city->datatables();
	}
	/* Datatables */
}
