<?php namespace App\Http\Controllers\Admin;

use App\Libs\Platform\Storage\Video\VideoRepository;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Admin\VideoValidationRequest;
use \File as File;

class VideoController extends AdminController {
	private $video;
	
	/**
	 * Constructor method - inject the Video Repository
	 * 
	 * @param App\Libs\Platform\Storage\Video\VideoRepository $video
	 */
	public function __construct(VideoRepository $video) {
		parent::__construct();
		
		$this->video = $video;
		$this->page->setActivePage('video');
		$this->page->setActiveSection(['video']);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		/* Default Variables */
		$active = 0;	// fetch only active entries or all entries
		$fields = [];	// list of fields to be fetched
		$filters = [];	// list of filters (where clause)
		$limit = 25;	// number of entries
		$metaDesc = 'List of Video';	// meta description
		$metaKeywords = 'list, video, list of video, video list';	// meta keywords
		$sort = ['name'];	// sorting of data
		$title = 'Video List';
		$with = ['category','city'];
		/* Default Variables */
		
		/* Input Variables */
		if (Input::has('active')) {
			$active = 1;
		}
		if (Input::has('fields')) {	// separator is comma
			$fields = explode(',', Input::get('fields'));
		}
		if (Input::has('limit')) {
			$limit = Input::get('limit');
		}
		if (Input::has('search')) {
			$filters['search'] = Input::get('search');
		}
		if (Input::has('sort')) {	// separator is comma
			$sort = explode(',', Input::get('sort'));
		}
		/* Input Variables */
		
		/* Get Data */
		$response = $this->video->listing($limit, $active, $fields, $filters, $sort, $with);
		/* Get Data */
		
		/* JSON Response for API */
		if ($this->platform->isApi()) {
			return response($response->toArray(), 200);
		}
		/* JSON Response for API */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Video', 'video', 'hq.video.index');
		$this->page->getBody()->addBreadcrumb('Listing');
		/* Breadcrumbs */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('list', $response);
		/* Set Data for View */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		/* Default Variables */
		$metaDesc = 'Create a new video';	// meta description
		$metaKeywords = 'create, new, video, new video, create video, create new video';	// meta keywords
		$title = 'Create Video';
		/* Default Variables */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Video', 'video');
		$this->page->getBody()->addBreadcrumb('Create');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(VideoValidationRequest $videoValidator){
		/* Separation & Limitations of Data By Models */
		$data = Input::only('name', 'src' , 'description', 'category_id', 'is_visible');
		$data['user_id'] = '1'; //Auth::user()->id;
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->video->create($data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('video/' . $mr->id);
		/* Redirect Based on Model Response */
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Details of video: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'Video: ';
		$with = ['category','city'];
		/* Default Variables */
		
		/* Get Video */
		$response = $this->video->view($id, $active, $fields,$with);
		/* Get Video */

		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'details, video details, ' . addslashes($response['name']) . ', ' . addslashes($response['name']) . ' details';
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('video', $response);
		/* Set Data for View */
		
		/* Tab Navigation */
//		$tabNav = App::make('Platform\Storage\Video\VideoRepository')->tabNavigation($id);
//		$this->page->getBody()->addToData('tabNav', $tabNav);
		/* Tab Navigation */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Video', '/video');
		$this->page->getBody()->addBreadcrumb($response['name'], '/video/' . $response['id'], 'admin.video.show');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Edit details of video: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'Edit Video: ';
		$with = ['city','category'];
		/* Default Variables */
		
		/* Get Video */
		$response = $this->video->view($id, $active, $fields, $with);
		/* Get Video */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'manage, edit, manage video, edit video, edit ' . addslashes($response['name']) . ', manage '. addslashes($response['name']);
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Video', '/video');
		$this->page->getBody()->addBreadcrumb($response['name'], '/video/' . $response['id']);
		$this->page->getBody()->addBreadcrumb('Edit');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* Set Data To View */
		$this->page->getBody()->addToData('video', $response);
		/* Set Data To View */

		/* HTML View Response */
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,VideoValidationRequest $videoValidator){
		/* Separation & Limitations of Data By Models */
		$data = Input::only('name', 'is_active');
		$data['created_by'] = '1_Sameer';
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->video->update($id,$data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('video/' . $mr->_id);
		/* Redirect Based on Model Response */
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		/* Query Creation & Fire */
		$this->video->delete($id);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return redirect('video');
		/* Redirect Based on Model Response */
	}

	/**
	 * Display the bulk upload page
	 * 
	 * @return Response
	 */
	public function bulkUpload() {
		
		/* Default Variables */
		$metaDesc = 'Bulk upload and insert videos';	// meta description
		$metaKeywords = 'bulk, upload, insert, bulk upload, bulk insert, video, bulk upload videos, bulk insert videos';	// meta keywords
		$title = 'Video Bulk Upload';
		/* Default Variables */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Video', '/video');
		$this->page->getBody()->addBreadcrumb('Bulk Upload');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return view($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Method to process the bulk upload file
	 * 
	 * @return Response
	 */
	public function bulkUploadProcess() {
		/* Default Variables */
		$csvFileValidator = new CSVFileValidator(10485760);	// for CSV file validation - max file size is 10MB
		$dataFile = Input::hasFile('dataFile')? Input::file('dataFile') : null;
		$errors = new MessageBag();
		$is_header = Input::has('is_header')? Input::get('is_header') : 0;
		/* Default Variables */
		
		/* File Validation */
		$errors->merge($csvFileValidator->validateFile($dataFile, 'dataFile'));	// validate the CSV file
		/* File Validation */
		
		if ($errors->count() > 0) {	// some validation failed
			return Redirect::to('video/bulk-upload')->withErrors($errors);
		}
		
		/* Save CSV File */
		$newCSVFileName = 'video_' . date('YmdHis') . '.' . File::extension($dataFile->getClientOriginalName());
		$dataFile->move(public_path($this->bulkuploadPath), $newCSVFileName);
		if ($dataFile->isValid()) {	// file couldn't be saved
			$errors->add('dataFile', 'Oops! Something went wrong with the file upload. Kindly try again.');
			return redirect('video/bulk-upload')->withErrors($errors);
		}
		/* Save CSV File */
		
		/* Local Variables */
		$archiveFile = new CSVReader(public_path($this->archiveFilePath . $newCSVFileName));
		$counter = 1;	// counter
		$columnCount = 2;	// correct number of columns in the CSV file
		$csvFile = new CSVReader($this->bulkuploadPath . $newCSVFileName);	// the uploaded (and saved) CSV file
		$errorCount = 0;	// number of failed inserts
		$insertCount = 0;	// number of successful inserts
		/* Local Variables */
		
		/* File Read and Insert */
		foreach ($csvFile as $row) {
			if ($is_header && $counter==1) {	// header is set and currently read 1 line
				$archiveFile->writeRow($row);
				$counter++;
				continue;
			}
			
			if (count($row) == $columnCount) {	// file should contain the right number of columns
				$data = [];
				$data['name'] = isset($row[0]) ? $row[0] : '';
				$data['is_active'] = isset($row[1]) ? $row[1] : '';
				
				$videoValidation = new VideoValidationRequest;
				$validation = \Validator::make($data,$videoValidation->rules());
				/* Query Creation & Fire */
				
				/* Redirect Based on Model Response */
				if ($validation->passes()) { // If Successful
					$mr = $this->video->create($data);
					$row[] = 'successful';	// add status to $row
					$archiveFile->writeRow($row);	// write to the archive file
					$insertCount++;
					$counter++;
				}
				else { // If Error
					$messages = $validation->messages();
					$row[] = 'fail';	// add status to $row
					$errorArray = [];
					foreach ($messages->all() as $k=>$v) {
						$errorArray[] = $k . ': ' . $v;
					}
					$row[] = implode('|', $errorArray);	// add message to $row
					
					$archiveFile->writeRow($row);	// write to the archive file
					$errorCount++;
					$counter++;
				}
				/* Redirect Based on Model Response */
			}
			else {	// file doesn't contain the right number of columns
				$row[] = 'fail';	// add status to $row
				$row[] = 'Column count must be ' . $columnCount;	// add message to $row
				$archiveFile->writeRow($row);	// write to the archive file
				$errorCount++;
				$counter++;
			}
		}
		/* File Read and Insert */
		
		/* Delete Uploaded File */
		unlink(public_path($this->bulkuploadPath . $newCSVFileName));
		/* Delete Uploaded File */
		
		return redirect('video/bulk-upload')->with(array('success'=>$insertCount . ' rows sucessfully inserted and ' . $errorCount . ' rows could not be inserted', 'file' => $this->archiveFilePath . $newCSVFileName));
	}
	
}
