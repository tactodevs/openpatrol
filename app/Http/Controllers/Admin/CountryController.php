<?php namespace App\Http\Controllers\Admin;

use \App as App;
use \Input as Input;
use \Lang as Lang;
use \App\Libs\Platform\Storage\Country\CountryRepository as CountryRepository;
use \Redirect as Redirect;
use \Response as Response;
use \View as View;
use \URL as URL;

class CountryController extends AdminController {
	private $country;
	
	/**
	 * Constructor method - inject the Country Repository
	 * 
	 * @param \App\Libs\Platform\Storage\Country\CountryRepository $country
	 */
	public function __construct(CountryRepository $country) {
		parent::__construct();
		
		$this->country = $country;
		$this->page->setActivePage('country');
		$this->page->setActiveSection(array('geo', 'country'));
	}
	
	/**
	 * Display a listing of the resource.
	 * In case of API call it will return JSON otherswise display a listing page
	 * 
	 * @return Response
	 */
	public function index() {
		
		/* Default Variables */
		$active = 0;	// fetch only active entries or all entries
		$fields = [];	// list of fields to be fetched
		$filters = [];	// list of filters (where clause)
		$metaDesc = 'List of countries';	// meta description
		$metaKeywords = 'list, countries, list of countries, country list';	// meta keywords
		$limit = 25;	// number of entries
		$sort = array('name');	// sorting of data
		$title = 'Countries Listing';
		$with = [];
		/* Default Variables */
		
		/* Input Variables */
		if (Input::has('active')) {
			$active = 1;
		}
		if (Input::has('fields')) {	// separator is comma
			$fields = explode(',', Input::get('fields'));
		}
		if (Input::has('limit')) {
			$limit = Input::get('limit');
		}
		if (Input::has('search')) {
			$filters['search'] = Input::get('search');
		}
		if (Input::has('sort')) {	// separator is comma
			$sort = explode(',', Input::get('sort'));
		}
		if (Input::has('with')) {	// separator is comma
			$with = explode(',', Input::get('with'));
		}
		/* Input Variables */
		
		/* Get Data for View */
		$response = $this->country->listing($limit, $active, $fields, $filters, $sort, $with);
		/* Get Data for View */
		
		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Country', 'country', 'admin.country.index');
		$this->page->getBody()->addBreadcrumb('Listing');
		/* Breadcrumbs */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('list', $response);
		/* Set Data for View */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Show the form for creating a new resource.
	 * 
	 * @return Response
	 */
	public function create() {
		/* Default Variables */
		$metaDesc = 'Create a new country';	// meta description
		$metaKeywords = 'create, new, country, new country, create country, create new country';	// meta keywords
		$title = 'Create Country';
		/* Default Variables */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Country', 'country');
		$this->page->getBody()->addBreadcrumb('Create');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */

		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Store a newly created resource in storage.
	 * 
	 * @return Response
	 */
	public function store() {
		/* Separation & Limitations of Data By Models */
		$data['country'] = Input::only('name', 'countrycode', 'currency_id', 'telcode', 'is_active');
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->country->create($data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return Redirect::to('country/' . $mr->id)->with(array('success' => Lang::get('messages.crud.create.success', array('action' => 'created'))));		
		/* Redirect Based on Model Response */
	}
	
	/**
	 * Display the specified resource.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function show($id) {
		
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Details of country: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'Country: ';
		$with = [];
		/* Default Variables */
		
		/* Get Country */
		$response = $this->country->view($id, $active, $fields, $with);
		/* Get Country */
		
		/* JSON View Response */
		if ($this->platform->isApi()) {
			return Response::json($response->toArray());
		}
		/* JSON View Response */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'details, country details, ' . addslashes($response['name']) . ', ' . addslashes($response['name']) . ' details';
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Set Data for View */
		$this->page->getBody()->addToData('country', $response);
		/* Set Data for View */
		
		/* Tab Navigation */
		$tabNav = App::make('App\Libs\Platform\Storage\Country\CountryRepository')->tabNavigation($id);
		$this->page->getBody()->addToData('tabNav', $tabNav);
		/* Tab Navigation */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Country', 'country');
		$this->page->getBody()->addBreadcrumb($response['name']);
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Show the form for editing the specified resource.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function edit($id) {
		
		/* Default Variables */
		$active = false;
		$fields = [];
		$metaDesc = 'Edit details of country: ';	// meta description
		$metaKeywords = '';	// meta keywords
		$title = 'Edit Country: ';
		$with = [];
		/* Default Variables */
		
		/* Get Country */
		$response = $this->country->view($id, $active, $fields, $with);
		/* Get Country */
		
		/* Enhance Meta Information and Title */
		$metaDesc .= addslashes($response['name']);
		$metaKeywords .= 'manage, edit, manage country, edit country, edit ' . addslashes($response['name']) . ', manage '. addslashes($response['name']);
		$title .= addslashes($response['name']);
		/* Enhance Meta Information and Title */
		
		/* Breadcrumbs */
		$this->page->getBody()->addBreadcrumb('Country', 'country');
		$this->page->getBody()->addBreadcrumb($response['name'], 'country/' . $response['id']);
		$this->page->getBody()->addBreadcrumb('Edit');
		/* Breadcrumbs */
		
		/* Page Maker */
		$this->page->getHead()->setDescription($metaDesc);
		$this->page->getHead()->setKeywords($metaKeywords);
		$this->page->setTitle($title);
		/* Page Maker */
		
		/* Set Data To View */
		$this->page->getBody()->addToData('country', $response);
		/* Set Data To View */

		/* HTML View Response */
		return View::make($this->viewBase . '.' . __FUNCTION__, array('page' => $this->page));
		/* HTML View Response */
	}
	
	/**
	 * Update the specified resource in storage.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function update($id) {
		/* Separation & Limitations of Data By Models */
		$data['country'] = Input::only('name', 'countrycode', 'currency_id', 'telcode', 'is_active');
		/* Separation & Limitations of Data By Models */
		
		/* Query Creation & Fire */
		$mr = $this->country->update($id, $data);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return Redirect::to('country/' . $id)->with(array('success' => Lang::get('messages.crud.update.success', array('action' => 'updated'))));
		/* Redirect Based on Model Response */
	}
	
	/**
	 * Remove the specified resource from storage.
	 * 
	 * @param int $id
	 * @return Response
	 */
	public function destroy($id) {
		/* Query Creation & Fire */
		$this->country->delete($id);
		/* Query Creation & Fire */
		
		/* Redirect Based on Model Response */
		return Redirect::to(URL::to('country'))->with(array('success' => Lang::get('messages.crud.delete.success', array('action' => 'deleted'))));
		/* Redirect Based on Model Response */
	}
}
