<?php namespace App\Http\Controllers\Frontend;

use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libs\Platform\Page\PageManager;
use Illuminate\Http\Request;

//use \Auth as Auth;
//use \Lang as Lang;

class FrontendController extends Controller {
	protected $page = null;	// PageManager object
	protected $platform;	// type of platform i.e. api or html (web)
	protected $viewBase = '';	// base directory path of the view file
	
	/**
	 * Constructor method
	 */
	public function __construct() {
		$this->platform = App::make('pf');
		
		/* Run Necessary Middleware */
		if (!$this->platform->isApi()) {	// if platform is not API
//			$this->beforeMiddleware('csrf', array('on' => array('delete', 'post', 'put')));
		}
		/* Run Necessary Middleware */
		
		$this->page = new PageManager();
		$this->page->setActivePage(str_replace('controller', '', strtolower(substr(get_class(), strrpos(get_class(), '\\') + 1))));
		$this->page->getBody()->addBreadcrumb('Home', '/');
		$this->viewBase = str_replace('\\', '.', str_replace('controller', '', str_replace('app\\http\\controllers\\', '', strtolower(get_called_class()))));
		$this->viewBase = str_replace('controller', '', $this->viewBase);
	}
	
	public function getCreator() {
		return Auth::user()->_id . '_' . Auth::user()->name;
	}
	
	
	/**
	 * Method to set the Layout
	 */
	protected function setupLayout() {
		if (!is_null($this->layout))
			$this->layout = View::make($this->layout);
	}
}
