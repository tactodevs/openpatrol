<?php

/* Variables */
$domain = 'openpatrol.com';
$subdomain = 'admin';
$webdomain = 'www';
/* Variables */

/* Singletons */
App::singleton('pf', function() { return new App\Libs\Platform\PlatformFactory(); });	// instance of platform factory
/* Singletons */


/*Admin Routes */
Route::group(['domain'=>$subdomain . '.' . $domain], function() {
	
	/* Bulk Upload Routes */
	get('tag/bulk-upload', 'Admin\TagController@bulkUpload');
	post('tag/bulk-upload', 'Admin\TagController@bulkUploadProcess');
	/* Bulk Upload Routes */
	
	/* Standard Web Routes */
	resource('category', 'Admin\CategoryController');
	resource('city', 'Admin\CityController');
	resource('country', 'Admin\CountryController');
	resource('tag', 'Admin\TagController');
	resource('video', 'Admin\VideoController');
	resource('law','Admin\LawController');
	resource('state', 'Admin\StateController');
	resource('user','Admin\UserController');
	/* Standard Web Routes */
	
	/* Other Routes */
//	get('download', 'Admin\DownloadController@index');	// route for downloading sample files
	/* Other Routes */
});
/*Admin Routes */


/* Web Routes */
Route::group(['domain'=>$webdomain . '.' . $domain], function() {
	get('/','Frontend\VideoController@index');
	
	get('upload-videos',['middleware' => 'auth', 'uses' => 'Frontend\VideoController@upload']);
	post('upload-videos',['middleware' => 'auth', 'uses' => 'Frontend\VideoController@store']);
	
	get('video/{name}','Frontend\VideoController@show');
	post('video/{name}','Frontend\VideoController@addLaw');
});
/* Web Admin Routes */

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);

/* API Routes */
Route::group(array('prefix'=>'api/v1', 'before'=>''), function () {
	resource('city', 'Admin\CityController');
	resource('state', 'Admin\StateController');
	resource('country', 'Admin\CountryController');
	resource('category', 'Admin\CategoryController');
	resource('law', 'Admin\LawController');
});
/* API Routes */
