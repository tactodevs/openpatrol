<!-- START :: Sidebar -->
<div class="page-sidebar-wrapper">
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- START :: Sidebar Menu -->
		<ul class="page-sidebar-menu " data-auto-scroll="true" data-slide-speed="200">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<li class="sidebar-toggler-wrapper" style="margin-bottom: 10px;">
				<!-- START :: Sidebar Toggler Button -->
				<div class="sidebar-toggler"></div>
				<!-- END :: Sidebar Toggler Button -->
			</li>
			<li class="start{!! ($page->getActiveSection(0) === 'dashboard')? ' active open' : '' !!}">
				<a href="{!! URL::to('/') !!}">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			
			<li class="start{!! ($page->getActiveSection(0) === 'category')? ' active open' : '' !!}">
				<a href="{!! URL::to('category') !!}">
					<i class="icon-layers"></i>
					<span class="title">Categories</span>
					<span class="selected"></span>
				</a>
			</li>
			
				
			<li class="start{!! ($page->getActiveSection(0) === 'video')? ' active open' : '' !!}">
				<a href="{!! URL::to('videos') !!}">
					<i class="icon-screen-tablet"></i>
					<span class="title">Videos</span>
					<span class="selected"></span>
				</a>
			</li>
			
			
			<li class="start{!! ($page->getActiveSection(0) === 'users')? ' active open' : '' !!}">
				<a href="{!! URL::to('users') !!}">
					<i class="icon-users"></i>
					<span class="title">Users</span>
					<span class="selected"></span>
				</a>
			</li>
			
			
			<li class="start{!! ($page->getActiveSection(0) === 'tag')? ' active open' : '' !!}">
				<a href="{!! URL::to('tag') !!}">
					<i class="icon-tag"></i>
					<span class="title">Tags</span>
					<span class="selected"></span>
				</a>
			</li>
					
			<!-- START :: Masters - RBAC Section -->
			<li {!! ($page->getActiveSection(0) === 'rbac')? 'class="active"' : '' !!}>
				<a href="javascript:;">
					<i class="icon-user-follow"></i>
					RBAC
					<span class="arrow{!! ($page->getActiveSection(1) === 'rbac')? ' open' : '' !!}"></span>
				</a>
				<ul class="sub-menu">
					<li {!! ($page->getActiveSection(2) === 'rbac-users') ? 'class="active"' : '' !!}>
						<a href="{!! URL::to('rbac-users') !!}"><i class="icon-user"></i> Rbac-Users</a>
					</li>
					<li {!! ($page->getActiveSection(2) === 'permission') ? 'class="active"' : '' !!}>
						<a href="{!! URL::to('permission') !!}"><i class="icon-pencil"></i> Permissions</a>
					</li>
					<li {!! ($page->getActiveSection(2) === 'role') ? 'class="active"' : '' !!}>
						<a href="{!! URL::to('role') !!}"><i class="icon-list"></i> Roles</a>
					</li>
					<li {!! ($page->getActiveSection(2) === 'role-permission') ? 'class="active"' : '' !!}>
						<a href="{!! URL::to('role-permission') !!}"><i class="icon-list"></i> Role Permission </a>
					</li>
				</ul>
			</li>
			<!-- END :: Masters - RBAC Section -->
		</ul>
		<!-- END :: Sidebar Menu -->
	</div>
</div>
<!-- END :: Sidebar -->