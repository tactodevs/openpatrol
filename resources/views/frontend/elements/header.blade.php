<?php
//	$user = \Auth::user()->toArray();
	$user = [
		'firstname' => 'G',
		'lastname' => 'D'
	];
?>
<!-- START :: Header -->
<div class="page-header navbar navbar-fixed-top">
	<!-- START :: Header Inner -->
	<div class="page-header-inner">
		<!-- START :: Logo -->
		<div class="page-logo">
			<a href="{!! URL::to('/') !!}">{!! HTML::image('assets/images/logo.jpg', 'logo', array('class'=>'logo-image', 'style'=>'padding-top:3px;')) !!}</a>
		</div>
		<!-- END :: Logo -->
		<!-- START :: Responsive Menu Toggler -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
		<!-- END :: Responsive Menu Toggler -->
		<!-- START :: Top Navigation Menu -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- START :: User Login Dropdown -->
				<li class="dropdown dropdown-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">{!! $user['firstname'] . ' ' . $user['lastname'] !!}</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="{!! URL::to('account/profile') !!}"><i class="icon-user"></i> My Profile </a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="{!! URL::to('account/lock') !!}"><i class="icon-lock"></i> Lock Screen </a>
						</li>
						<li>
							<a href="{!! URL::to('account/logout') !!}"><i class="icon-key"></i> Log Out </a>
						</li>
					</ul>
				</li>
				<!-- END :: User Login Dropdown -->
			</ul>
		</div>
		<!-- END :: Top Navigation Menu -->
	</div>
	<!-- END :: Header Inner -->
</div>
<!-- END :: Header -->
<div class="clearfix"></div>
