<?php $breadcrumbs = $page->getBody()->getBreadcrumbs(); ?>
@if ($breadcrumbs)
	<div class="page-bar">
		<ul class="page-breadcrumb">
		@foreach ($breadcrumbs as $key => $value)
			<li>
			@if ($key == 0)
				<i class="fa fa-home"></i>
			@endif
			@if($value['url'])
				<a href="{{ $value['url'] }}">
			@endif
				{{ $value['title'] }}
			@if($value['url'])
				</a>
			@endif
			@if($key != (count($breadcrumbs)-1))
				<i class="fa fa-angle-right"></i>
			@endif
			</li>
		@endforeach
		@yield('breadcrumb-content')
		</ul>
	</div>
@endif