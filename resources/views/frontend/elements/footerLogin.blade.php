		<!-- START :: Footer -->
		<!-- START :: Copyright -->
		<div class="copyright">
			{!! date('Y') !!} &copy; {!! Config::get('app.title') !!}
		</div>
		<!-- END :: Copyright -->
		<!-- END :: Footer -->
	</div>
	@include('admin/elements/javascript')
</body>
</html>
