<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- START :: Head -->
<head>
	<meta charset="utf-8" />
	<title>{{ $page->getHead()->getTitle() }}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="{{ $page->getHead()->getDescription() }}" />
	<meta name="keywords" content="{{ $page->getHead()->getKeywords() }}" />
	<link rel="canonical" href="{{ url('/') }}/" />
	<link rel="shortcut icon" href="{{ URL::to('favicon.ico') }}" />
	<!-- START :: Global Plugin Styles -->
	{{ HTML::style('assets/css/fonts.googleapis.css') }}
	{{ HTML::style('assets/metronic/global/plugins/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('assets/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css') }}
	{{ HTML::style('assets/metronic/global/plugins/bootstrap/css/bootstrap.min.css') }}
	{{ HTML::style('assets/metronic/global/plugins/uniform/css/uniform.default.css') }}
	{{ HTML::style('assets/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}
	<!-- END :: Global Plugin Styles -->
	<!-- START :: Page Specific Plugin CSS -->
	@yield('pageHeadSpecificPluginCSS')
	<!-- END :: Page Specific Plugin CSS -->
	<!-- START :: Page Specific CSS -->
	@yield('pageHeadSpecificCSS')
	<!-- END :: Page Specific CSS -->
	<!-- START :: Theme CSS -->
	{{ HTML::style('assets/metronic/global/css/components.css') }}
	{{ HTML::style('assets/metronic/global/css/plugins.css') }}
	{{ HTML::style('assets/metronic/admin/layout/css/layout.css') }}
	{{ HTML::style('assets/metronic/admin/layout/css/themes/default.css') }}
	{{ HTML::style('assets/metronic/admin/layout/css/custom.css') }}
	<!-- END :: Theme CSS -->
	<!-- Load JS files at bottom to reduce page load time - see footer.blade.php -->
</head>
<!-- END :: Head -->
<!-- START :: Body -->
<body class="login">
