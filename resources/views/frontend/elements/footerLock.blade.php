		<!-- START :: Footer -->
		<!-- START :: Copyright -->
		<div class="page-footer-custom">
			 {!! date('Y') !!} &copy; {!! Config::get('app.title') !!}
		</div>
		<!-- END :: Copyright -->
		<!-- START :: Footer -->
	</div>
	@include('admin/elements/javascript')
</body>
</html>