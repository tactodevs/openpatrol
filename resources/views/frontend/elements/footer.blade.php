	<!-- START :: Footer -->
	<div class="page-footer">
		<!-- START :: Copyright -->
		<div class="page-footer-inner">
			{!! date('Y') !!} &copy; {!!Config::get('app.title') !!}
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
		<!-- END :: Copyright -->
	</div>
	<!-- END :: Footer -->
	@include('admin/elements/javascript')
</body>
</html>
