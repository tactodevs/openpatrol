@extends('frontend.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
	
@stop

@section('bodyContent')	{{-- Page Body Content --}}
@include('admin/elements/javascript')

{!! HTML::style('assets/css/style.css') !!}
<?php
	$items = $page->getBody()->getDataByKey('list')->getCollection()->toArray();
	$categories = $page->getBody()->getDataByKey('category');
	$cities = $page->getBody()->getDataByKey('city');
	$tags = $page->getBody()->getDataByKey('tags');
?>
<!-- START :: Managed Table -->
		<!---start-content---->
		<div class="main-content">
			<div class="wrap">
			<div class="left-sidebar">
				<div class="sidebar-boxs">
					<div class="sidebar-box">
					
					</div>
					<div class="clear"> </div>
					<div class="type-videos">
						<h3>Video Filters</h3>
						{!! Form::open(['method'=>'get','url'=>\URL::full(),'id'=>'filterForm']) !!}
						<?php 
						$val='false';
						if(Input::has('list')){
							$val = Input::get('list');
						}
						?>
						
						{!! Form::hidden('list',$val,['id'=>'listGrid']) !!}
						<?php 
							if(Input::has('page')){
								echo '<input type="hidden" name="page" value="'.Input::get('page').'" ';
							}
						?>
						
						<ul>
							<div>By Category</div>
							<div style="margin:4% 10%">
								<ul>
									@foreach($categories as $cat)
										<?php 
										$checked = '';
										if(Input::has('category')){
											if(in_array($cat['id'],Input::get('category'))){
												$checked = 'checked';
											}
										}
										?>
										<li><input class="formSubmit" value={{$cat['id'] }} {{ $checked }} name ="category[]" type="checkbox">{{ $cat['name']}}</li>	
									@endforeach
														
								</ul>
							</div>
						</ul>
						<ul>
							<div>By Date Added</div>
							<div style="margin:4% 10%">
								<?php 
									$d1='';$d2='';$d3='';$d4='';$d5='';
									if(Input::has('time')){
										if(in_array(date("m-Y"), Input::get('time'))){
											$d1 = 'checked';
										}
										if(in_array(date("m-Y",strtotime("-31 days")), Input::get('time'))){
											$d2 = 'checked';
										}
										if(in_array(date("m-Y",strtotime("-61 days")), Input::get('time'))){
											$d3 = 'checked';
										}
										if(in_array(date("m-Y",strtotime("-91 days")), Input::get('time'))){
											$d4 = 'checked';
										}
										if(in_array(date("m-Y",strtotime("-121 days")), Input::get('time'))){
											$d5 = 'checked';
										}
									}
								?>
								<ul>
									<li><input class="formSubmit" {{$d1}} value={{ date("m-Y") }} name="time[]" type="checkbox">{{ date("F Y") }}</li>	
									<li><input class="formSubmit" {{$d2}} value={{ date("m-Y",strtotime("-31 days")) }} name="time[]" type="checkbox">{{ date("F Y",strtotime("-31 days")) }}</li>	
									<li><input class="formSubmit" {{$d3}} value={{ date("m-Y",strtotime("-61 days")) }} name="time[]" type="checkbox">{{ date("F Y",strtotime("-61 days")) }}</li>	
									<li><input class="formSubmit" {{$d4}} value={{ date("m-Y",strtotime("-91 days")) }} name="time[]" type="checkbox">{{ date("F Y",strtotime("-91 days")) }}</li>	
									<li><input class="formSubmit" {{$d5}} value={{ date("m-Y",strtotime("-121 days")) }} name="time[]" type="checkbox">{{ date("F Y",strtotime("-121 days")) }}</li>								
								</ul>
							</div>
						</ul>
						<ul>
							<div>By Location</div>
							<div style="margin:4% 10%">
								<ul>
									@foreach($cities as $city)
										<?php 
										$checked = '';
										if(Input::has('city')){
											if(in_array($city['id'],Input::get('city'))){
												$checked = 'checked';
											}
										}
										?>
										<li><input class="formSubmit" name="city[]" {{ $checked }} value={{$city['id'] }} name ="city" type="checkbox">{{ $city['name']}}</li>	
									@endforeach							
								</ul>
							</div>
						</ul>
						<ul>
							<div class="filterByTag">By Tags</div>
							<div style="margin:4% 10%">
								<div id="tagInputs">
									<?php 
									if(Input::has('tags')){
										$InputTags = Input::get('tags');
										foreach ($InputTags as $tag){
											echo '<input class="someTag" type="hidden" name="tags[]" value="'.$tag.'"/>';
										}
									}
									?>
									
								</div>
								<?php 
								
								for($i=0;$i<count($tags);$i++){
									$style = 'style="cursor:pointer"';
									if(Input::has('tags')){
										if(in_array($tags[$i]['id'],Input::get('tags'))){
											$style = 'style="cursor:pointer;text-decoration:underline"';
										}
									}
									if($i%2 == 0){
										echo '<b '.$style.'><span  class="tags formSubmit" id="'.$tags[$i]['id'].'" style="font-size:25px">'.$tags[$i]['name'].'</span></b>  ';
									}else if($i%3 == 0){
										echo '<b '.$style.'><span  class="tags formSubmit" id="'.$tags[$i]['id'].'" style="font-size:20px">'.$tags[$i]['name'].'</span></b>  ';
									}
									else{
										echo '<b '.$style.'><span class="tags formSubmit" id="'.$tags[$i]['id'].'" style="font-size:14px">'.$tags[$i]['name'].'</span></b>  ';
									}
								}
								?>
							</div>
						</ul>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			<div class="right-content">
				<div class="right-content-heading">
					<div class="right-content-heading-left">
						<h3>Latest Colletcions of videos</h3>
					</div>
					<div class="right-content-heading-right">
						<h3><a class="formSubmit" id="gridView"  href="javascript:void(0)">Grid View</a> | 
							<a class="formSubmit" id="listView" href="javascript:void(0)" >List View</a></h3>
					</div>
					<div class="clear"> </div>
				</div>
				<div class="content-grids" >
					@if($items)
						@foreach($items as $video)
							@if(!Input::has('list') OR Input::get('list') == 'false')
							<div class="content-grid" style="margin-right: 2px !important">
								<a href="/video/{{$video['slug']}}"><img width="220" height="200" src="assets/images/video_thumbnails/{{$video['thumbnail']}}" title="allbum-name" /></a>
								<h3>{{ substr($video['description'],0,20) }}...</h3>
								<a class="button" href="/video/{{$video['slug']}}">Watch now</a>
							</div>
							@else
							<div class="content-grid" style="margin-right: 2px !important;width:100%">
								<div style="float:left">
									<a href="/video/{{$video['slug']}}">
										<img  width="220" height="200" src="assets/images/video_thumbnails/{{$video['thumbnail']}}" title="allbum-name" />
									</a>
								</div>
								<div style="float:left;margin: 5px;">
									<h3 style="text-align:left;font-weight: bold">{{ $video['name'] }}</h3>
									<h3 style="text-align:left">{{ substr($video['description'],0,200) }}...</h3>
									<h3 style="text-align:left">Uploaded on {{ date("F jS, Y",strtotime($video['created_at'])) }}</h3>
									<a style="width:10em" class="button" href="/video/{{$video['slug']}}">Watch now</a>
								</div>
								
							</div>
							@endif
						@endforeach
					@else
					<h2> No Results found</h2>
					@endif
					@include('admin.elements.pagination')
				
					<div class="clear"> </div>
					<!---End-pagenation----->
				</div>
			</div>
			<div class="clear"> </div>
			</div>
		</div>
		<div class="clear"> </div>
		
	</body>
</html>


<!-- END :: Managed Table -->
<script>
	
	$(document).ready(function(){
		$('#gridView').click(function(){
			$('#listGrid').val('false');
		});
		$('#listView').click(function(){
			$('#listGrid').val('true');
		});
		
		$('.tags').click(function(){
			var ids=[];
			i=0;
			$(this).css('text-decoration','underline');
			var id = $(this).attr('id');
			$('.someTag').each(function(){
				ids[i++]=$(this).val();
			});
			if(ids.indexOf(id) == -1){
				$('#tagInputs').append('<input class="someTag" type="hidden" name="tags[]" value="'+id+'"/>');
			}else{
				$('.someTag').each(function(){
					if($(this).val() == id){
						$(this).remove();
						$('.tags#'+id).css('text-decoration','none');
					}
				});
			
			}
		});
		
		$('.formSubmit').click(function(){
			$('#filterForm').submit();
		});
	});
	
</script>
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
	
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/frontend/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/corporate/table-managed.js') !!}
	{!! HTML::script('assets/scripts/general/ui-alert-dialog-api.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	
@stop
