@extends('frontend.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
	
@stop
@section('bodyContent')	{{-- Page Body Content --}}


<?php 
	$video = $page->getBody()->getDataByKey('video');
	$law = $page->getBody()->getDataByKey('law');
	$BrokenLaw = $page->getBody()->getDataByKey('BrokenLaw')->getCollection()->toArray();
?>
{!! HTML::style('assets/metronic/global/plugins/select2/select2.css') !!}
{!! HTML::style('assets/css/style.css') !!}
<div id="fb-root"></div>
{!! HTML::script('assets/scripts/fb_comments.js') !!}
	<body>	
		</div>
		<div class="clear"> </div>
		<!---start-content---->
		<div class="main-content">
			<div class="wrap">
			<div class="left-sidebar">
				<div class="sidebar-boxs">
					<div class="clear"> </div>
					<div class="type-videos">
						<h3>Laws Broken</h3>
						<ul>
							@if($BrokenLaw)
								@foreach($BrokenLaw as $brokenlaw)
								<li>Name : <b>{{ $brokenlaw['law']['name'] }}</b><br>
									Time : <b>{{ round($brokenlaw['time']) }} Seconds </b>
									</li>
									<hr>
								@endforeach
							@else
							<h2>No Broken Laws recorded</h2>
							@endif							
						</ul>
						
						<h3>Add</h3>
						{!! Form::open() !!}
						<div>
							<?php
							$lawId = ($law)? $law['id'] : '';
							$lawName = ($law)? $law['name']  : '';
							?>
							{!! Form::hidden('law_id', $lawId, array('id'=>'select2_law_id', 'data-text'=>$lawName, 'class'=>'form-control select2')) !!}
						</div>
						<div>
							{!! Form::hidden('video_id',$video['id']) !!}
						</div>
						<div>
							<?php 
							$city_id = Input::old('city_id');
							$category_id = Input::old('category_id');
							if($video['category_id']){
								$category_id = $video['category_id'];
							}
							if($video['city_id']){
								$city_id = $video['city_id'];
							}
							?>
							{!! Form::hidden('file_name',$video['src']) !!}
							{!! Form::hidden('time',null,['id'=>'time']) !!}
							{!! Form::hidden('city_id',$city_id,['id'=>'city_id']) !!}
							{!! Form::hidden('category_id',$category_id,['id'=>'category_id']) !!}
						</div>
						<div>
							{!! Form::submit() !!}
						</div>
						{!! Form::close() !!}
						
					</div>
				</div>
			</div>
			<div class="right-content">
				<div class="right-content-heading">
					<div class="right-content-heading-left">
						<h3>Video : {{ $video['name']}}</h3>
					</div>
					<div class="right-content-heading-right">
						<div class="social-icons">
			                <ul>
			                    <li><a class="facebook" href="#" target="_blank"> </a></li>
			                    <li><a class="twitter" href="#" target="_blank"></a></li>
			                    <li><a class="googleplus" href="#" target="_blank"></a></li>
			                    <li><a class="pinterest" href="#" target="_blank"></a></li>
			                    <li><a class="dribbble" href="#" target="_blank"></a></li>
			                    <li><a class="vimeo" href="#" target="_blank"></a></li>
			                </ul>
					</div>
					</div>
					<div class="clear"> </div>
				</div>
				<div class="inner-page">
				<div class="title">
					<h3>{{ $video['description'] }} </h3>
				</div>
				<div class="video-inner">
					<video id="video" src="/assets/videos/{{ $video['src']}}" width="100%" height="500px" controls></video>
				</div>
				<div class="clear"> </div>
				<div class="video-details">
					<ul>
						<li><p>Uploaded on <a href="#">{{ date("F jS, Y",strtotime($video['created_at'])) }}</a></p></li>
						<li><span>{{ $video['description'] }}</span></li>
					</ul>
				</div>
				<div class="clear"> </div>
				<div class="tags">
					<ul>
						<li><h3>Tags:</h3></li>
						@foreach($video['video_tag'] as $tag)
						<li><a href=/?tags%5B%5D={{$tag['tag']['id'] }} >{{ $tag['tag']['name'] }}</a>  </li>
						@endforeach
			
					</ul>
				</div>
				<div class="clear"> </div>
				
		  		<div class="artical-commentbox">
		  						 	
		  		<div class="table-form">
					
				<div class="fb-comments" data-href={{ \Request::url() }} data-numposts="3" data-colorscheme="light"></div>
				</div>
		  		</div>
			</div>
			</div>
			<div class="clear"> </div>
			</div>
		</div>
		<div class="clear"> </div>
		
	</body>

	<script>
		var myVid=document.getElementById("video");
		myVid.onpause = function() {
			document.getElementById('time').value=myVid.currentTime;
		};
		
	</script>
<!-- END :: Managed Table -->
@include('admin/elements/javascript')

{!! HTML::script('assets/metronic/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('assets/scripts/law/components-dropdowns.js') !!}
<script>
		jQuery(document).ready(function() {
			ComponentsDropdowns.init();
		});
</script>
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
	
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/frontend/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/corporate/table-managed.js') !!}
	{!! HTML::script('assets/scripts/general/ui-alert-dialog-api.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	
@stop
