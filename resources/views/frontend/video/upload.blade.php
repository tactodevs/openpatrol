@extends('frontend.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
<?php
	$location = $page->getBody()->getDataByKey('location');
?>
<style>
	#map-canvas {
		width: 50%;
        height: 50%;
        margin: 0px;
        padding: 0px
      }
</style>
<!-- START :: Form -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Upload Video
		</div>
	</div>
	<div class="portlet-body form">
		{!! Form::open(array('name'=>'create', 'files'=>true, 'method'=>'POST', 'url'=>'upload-videos', 'class'=>'form-horizontal')) !!}
			<div class="form-body">
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('name'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Name
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('name', Input::old('name'), array('class'=>'form-control')) !!}
								@if ($errors->has('name'))
								<span class="help-block">{!! $errors->first('name') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('src'))? ' has-error':'' !!}">
							<label for="src" class="control-label col-md-3">
								Video
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::file('src', array('class'=>'form-control')) !!}
								@if ($errors->has('src'))
								<span class="help-block">{!! $errors->first('src') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('description'))? ' has-error':'' !!}">
							<label for="description" class="control-label col-md-3">
								Description
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('description', Input::old('description'), array('class'=>'form-control')) !!}
								@if ($errors->has('description'))
								<span class="help-block">{!! $errors->first('description') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('category_id'))? ' has-error':'' !!}">
							<label for="category_id" class="control-label col-md-3">
								Category
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('category_id', Input::old('category_id'), array('class'=>'form-control')) !!}
								@if ($errors->has('category_id'))
								<span class="help-block">{!! $errors->first('category_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('tags'))? ' has-error':'' !!}">
							<label for="tags" class="control-label col-md-3">
								Tags
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('tags', Input::old('tags'), array('class'=>'form-control')) !!}
								@if ($errors->has('tags'))
								<span class="help-block">{!! $errors->first('tags') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				<div class="form-group">
					<label for='map'>Location</label>
					<input type="text" id="searchmap">
					<div id="map-canvas"></div>
				</div>
				<div class="form-group">
					<label for='lat'>Lat</label>
					<input type="text" id="lat" name="lat">
				</div>
				<div class="form-group">
					<label for='lat'>Lng</label>
					<input type="text" id="lng" name="lng">
				</div>
				<div class="form-group">
					<label for='city'>City</label>
					<input type="text" id="city" name="city">
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue"><i class="fa fa-save"></i> Save</button>
								<a href="{!! URL::previous() !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		{!! Form::close() !!}
	
	</div>
</div>
<script>
	if(navigator.geolocation) {
    browserSupportFlag = true;
    navigator.geolocation.getCurrentPosition(function(position) {
      initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
      map.setCenter(initialLocation);
    }, function() {
      handleNoGeolocation(browserSupportFlag);
	});
	}
	var map = new google.maps.Map(document.getElementById('map-canvas'),{
		center:{
			lat:1.0036125,
			lng:7.8317809
		},
		zoom:15
	})
	var marker = new google.maps.Marker({
		position:{
			lat:19.0036125,
			lng:72.8317809
		},
		map:map,
		draggable:true
	});
	if(navigator.geolocation) {
    browserSupportFlag = true;
    navigator.geolocation.getCurrentPosition(function(position) {
      initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
      map.setCenter(initialLocation);
    }, function() {
      handleNoGeolocation(browserSupportFlag);
	});
	}
	
	var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
	
	google.maps.event.addListener(searchBox,'places_changed',function(){
		var places = searchBox.getPlaces();
		var bounds = new google.maps.LatLngBounds();
		var i,place;
		for(i=0;place=places[i];i++){
			bounds.extend(place.geometry.location);
			marker.setPosition(place.geometry.location);
		}
		map.fitBounds(bounds);
		map.setZoom(15);
	});
	
	
	google.maps.event.addListener(marker,'position_changed',function(){
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();
		var latlng = new google.maps.LatLng(lat, lng);

		geocoder = new google.maps.Geocoder();
		geocoder.geocode(
		{'latLng': latlng}, 
			function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						var add= results[0].formatted_address ;
						var  value=add.split(",");
						count=value.length;
						city=value[count-3];
					   $('#city').val(city);
					}  
				}
			}
		);
		$('#lat').val(lat);
		$('#lng').val(lng);
	});
</script>
<!-- END :: Form -->
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
@stop
