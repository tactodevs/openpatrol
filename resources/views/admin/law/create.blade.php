@extends('admin.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
{!! HTML::style('assets/metronic/global/plugins/select2/select2.css') !!}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
<?php
	$city = $page->getBody()->getDataByKey('city');
?>
<!-- START :: Form -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Create Law
		</div>
	</div>
	<div class="portlet-body form">
		{!! Form::open(array('name'=>'create', 'method'=>'POST', 'url'=>'law', 'class'=>'form-horizontal')) !!}
			<div class="form-body">
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('name'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Name
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('name', Input::old('name'), array('class'=>'form-control')) !!}
								@if ($errors->has('name'))
								<span class="help-block">{!! $errors->first('name') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('category_id'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Category
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::hidden('category_id', Input::old('category_id'), array('id'=>'select2_category_id', 'data-text'=>'', 'class'=>'form-control select2')) !!}
								@if ($errors->has('category_id'))
								<span class="help-block">{!! $errors->first('category_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('description'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Description
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::textarea('description', Input::old('description'), array('class'=>'form-control')) !!}
								@if ($errors->has('description'))
								<span class="help-block">{!! $errors->first('description') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('keywords'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Keywords
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::textarea('keywords', Input::old('keywords'), array('class'=>'form-control')) !!}
								@if ($errors->has('keywords'))
								<span class="help-block">{!! $errors->first('keywords') !!}</span>
								@endif
								<span>Note : Enter each keyword on a new line</span>
							</div>
							
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				<!-- START :: row -->
				<div class="row">
					<?php
						$cityId = ($city)? $city['id'] : '';
						$cityName = ($city)? $city['name'] . ', ' . $city['state']['name'] : '';
					?>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('city_id'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Cities
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::hidden('city_id', $cityId, array('id'=>'select2_city_id', 'data-text'=>$cityName, 'class'=>'form-control select2')) !!}
								@if ($errors->has('category_id'))
								<span class="help-block">{!! $errors->first('city_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<?php
							$is_active = Input::old('is_active');
							if ($is_active == '')
								$is_active = 1;
						?>
						<div class="form-group{!! ($errors->has('is_active'))? ' has-error':'' !!}">
							{!! Form::label('is_active', 'Active', array('class'=>'control-label col-md-3')) !!}
							<div class="col-md-9">
								<div class="radio-list">
									<label class="radio-inline">
										<input type="radio" name="is_active" value="1" <?php if ($is_active == 1) { echo 'checked="checked"'; } ?> /> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="is_active" value="0" <?php if ($is_active == 0) { echo 'checked="checked"'; } ?>/> No
									</label>
								</div>
								@if ($errors->has('is_active'))
								<span class="help-block">{!! $errors->first('is_active') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			<!-- END :: row -->
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue"><i class="fa fa-save"></i> Save</button>
								<a href="{!! URL::previous() !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<!-- END :: Form -->
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
{!! HTML::script('assets/metronic/global/plugins/select2/select2.min.js') !!}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/law/components-dropdowns.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			ComponentsDropdowns.init();
		});
	</script>
@stop
