<?php 
	$paginator = $page->getBody()->getDataByKey('list');
	$queryStrParameters = \Input::all();
	if (array_key_exists('page', $queryStrParameters))
		unset($queryStrParameters['page']);
	
	foreach ($queryStrParameters as $key=>$value)
		$paginator->appends($key, $value);
	
	$presenter = new Illuminate\Pagination\BootstrapThreePresenter($paginator);
?>
@if ($paginator->count())
<div class="row">
	<div class="col-md-5 col-sm-12">
		<div class="dataTables_info" id="managed_table_1_info" role="status" aria-live="polite">
			Showing {!!$paginator->firstItem()!!} to {!!$paginator->lastItem()!!} of {!!$paginator->total()!!} entries
		</div>
	</div>
	<div class="col-md-7 col-sm-12">
		<div class="dataTables_paginate paging_bootstrap_full_number" id="managed_table_area_paginate">
			<ul class="pagination" style="visibility:visible;">
				{!! $presenter->render(); !!}
			</ul>
		</div>
	</div>
</div>
@endif