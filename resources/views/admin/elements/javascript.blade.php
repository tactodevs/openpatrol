	<!-- START :: Core Plugins -->
	<!--[if lt IE 9]>
	{!! HTML::script('assets/metronic/global/plugins/respond.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/excanvas.min.js') !!}
	<![endif]-->
	{!! HTML::script('assets/metronic/global/plugins/jquery-1.11.0.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/jquery-migrate-1.2.1.min.js') !!}
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	{!! HTML::script('assets/metronic/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/bootstrap/js/bootstrap.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/jquery.blockui.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/jquery.cokie.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/uniform/jquery.uniform.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}
	<!-- END :: Core Plugins -->
	<!-- START :: Page Level Plugins -->
	@yield('pageFooterSpecificPlugin')
	<!-- END :: Page Level Plugins -->
	<!-- START :: Page Level Scripts -->
	@yield('pageFooterSpecificJS')
	<!-- END :: Page Level Scripts -->
	<!-- START :: Page Level Script Initialize -->
	@yield('pageFooterScriptInitialize')
	<!-- END :: Page Level Script Initialize -->
