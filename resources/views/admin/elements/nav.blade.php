<?php
	$tabNav = $page->getBody()->getDataByKey('tabNav');
?>
@if($tabNav)
	<!-- START :: Navigation Tabs -->
	<ul class="nav nav-tabs">
	@foreach($tabNav as $tab)
		<li {!! ($page->getActivePage() === $tab['key'])? ' class="active"' : '' !!}>
			<a href="{!! ($page->getActivePage() != $tab['key'])? URL::to($tab['url']) : '' !!}">{!! $tab['name'] !!}</a>
		</li>
	@endforeach
	</ul>
	<!-- START :: Navigation Tabs -->
@endif
