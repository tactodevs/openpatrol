@extends('admin.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
	{!! HTML::style('assets/metronic/global/plugins/select2/select2.css') !!}
	{!! HTML::style('assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') !!}

@stop

@section('bodyContent')	{{-- Page Body Content --}}
<?php
	$video = $page->getBody()->getDataByKey('video');
?>
<!-- START :: Form -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-edit"></i>Edit Video: {!! $video['name'] !!}
		</div>
	</div>
	<div class="portlet-body form">
		{!! Form::model($video, array('name'=>'edit', 'method'=>'PUT', 'url'=> 'video/' . $video['id'], 'class'=>'form-horizontal')) !!}
			<div class="form-body">
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('name'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
							Name
							<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('name', Input::old('name'), array('class'=>'form-control')) !!}
								@if ($errors->has('name'))
								<span class="help-block">{!! $errors->first('name') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<?php
						$categoryId = ($video['category'])? $video['category']['id'] : '';
						$categoryName = ($video['category'])? $video['category']['name'] : '';
						?>
						<div class="form-group{!! ($errors->has('category_id'))? ' has-error':'' !!}">
							<label for="src" class="control-label col-md-3">
							Category
							<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::hidden('category_id', $categoryId, array('id'=>'select2_category_id', 'data-text'=>$categoryName, 'class'=>'form-control select2')) !!}
								@if ($errors->has('category_id'))
								<span class="help-block">{!! $errors->first('category_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('description'))? ' has-error':'' !!}">
							<label for="description" class="control-label col-md-3">
							Description
							<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::textarea('description', Input::old('description'), array('class'=>'form-control')) !!}
								@if ($errors->has('description'))
								<span class="help-block">{!! $errors->first('description') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<label for="src" class="control-label col-md-3">
								Video
						<span class="required" aria-required="true">*</span>
						</label>
						<div class="col-md-9">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
									{!! HTML::image('assets/images/video_thumbnails/'.$video['thumbnail'],'') !!}
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
								<div>
									<span class="btn default btn-file">
										<span class="fileinput-new"> Select Video </span>
										<span class="fileinput-exists"> Change </span>
										{!! Form::file('src') !!}
									</span>
									<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
								</div>
							</div>
							@if ($errors->has('src'))
							<span class="help-block">{!! $errors->first('src') !!}</span>
							@endif
						</div>
					</div>
					
				</div>
				<!-- END :: row -->
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<?php
						$cityId = ($video['city'])? $video['city']['id'] : '';
						$cityName = ($video['city'])? $video['city']['name'] : '';
						?>
						<div class="form-group{!! ($errors->has('city_id'))? ' has-error':'' !!}">
							<label for="src" class="control-label col-md-3">
							Category
							<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::hidden('city_id', $cityId, array('id'=>'select2_city_id', 'data-text'=>$cityName, 'class'=>'form-control select2')) !!}
								@if ($errors->has('city_id'))
								<span class="help-block">{!! $errors->first('city_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<?php
							$is_visible = Input::old('is_visible');
							if ($is_visible == '')
								$is_visible = $video['is_visible'];
						?>
						<div class="form-group{!! ($errors->has('is_visible'))? ' has-error':'' !!}">
							{!! Form::label('is_visible', 'Active', array('class'=>'control-label col-md-3')) !!}
							<div class="col-md-9">
								<div class="radio-list">
									<label class="radio-inline">
										<input type="radio" name="is_visible" value="1" <?php if ($is_visible == 1) { echo 'checked="checked"'; } ?> /> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="is_visible" value="0" <?php if ($is_visible == 0) { echo 'checked="checked"'; } ?>/> No
									</label>
								</div>
								@if ($errors->has('is_visible'))
								<span class="help-block">{!! $errors->first('is_visible') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue"><i class="fa fa-save"></i> Save</button>
								<a href="{!! URL::previous() !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			{!! Form::hidden('revision', $video['revision']) !!}
		{!! Form::close() !!}
	</div>
</div>
<!-- END :: Form -->
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
	{!! HTML::script('assets/metronic/global/plugins/select2/select2.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') !!}

@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/video/components-dropdowns.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			ComponentsDropdowns.init();
		});
	</script>
@stop
