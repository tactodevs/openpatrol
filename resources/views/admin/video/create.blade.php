@extends('admin.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
<!-- START :: Form -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Create Tag
		</div>
	</div>
	<div class="portlet-body form">
		{!! Form::open(array('name'=>'create', 'method'=>'POST', 'url'=>'tag', 'class'=>'form-horizontal')) !!}
			<div class="form-body">
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('name'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Name
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('name', Input::old('name'), array('class'=>'form-control')) !!}
								@if ($errors->has('name'))
								<span class="help-block">{!! $errors->first('name') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('src'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Video
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::file('src', Input::old('src'), array('class'=>'form-control')) !!}
								@if ($errors->has('src'))
								<span class="help-block">{!! $errors->first('src') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('description'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Description
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('description', Input::old('description'), array('class'=>'form-control')) !!}
								@if ($errors->has('description'))
								<span class="help-block">{!! $errors->first('description') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('category_id'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Category
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('category_id', Input::old('category_id'), array('class'=>'form-control')) !!}
								@if ($errors->has('category_id'))
								<span class="help-block">{!! $errors->first('category_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<?php
							$is_visible = Input::old('is_visible');
							if ($is_visible == '')
								$is_visible = 1;
						?>
						<div class="form-group{!! ($errors->has('is_visible'))? ' has-error':'' !!}">
							{!! Form::label('is_visible', 'Visibility', array('class'=>'control-label col-md-3')) !!}
							<div class="col-md-9">
								<div class="radio-list">
									<label class="radio-inline">
										<input type="radio" name="is_visible" value="1" <?php if ($is_visible == 1) { echo 'checked="checked"'; } ?> /> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="is_visible" value="0" <?php if ($is_visible == 0) { echo 'checked="checked"'; } ?>/> No
									</label>
								</div>
								@if ($errors->has('is_visible'))
								<span class="help-block">{!! $errors->first('is_visible') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			<!-- END :: row -->
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue"><i class="fa fa-save"></i> Save</button>
								<a href="{!! URL::previous() !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<!-- END :: Form -->
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
		});
	</script>
@stop
