@extends('hq.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
	{!! HTML::style('assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') !!}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
<!-- START :: Form -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-file-o"></i>Tag Bulk Upload
		</div>
	</div>
	<div class="portlet-body form">
		{!! Form::open(array('name'=>'create', 'files'=>true, 'method'=>'POST', 'url'=>'tag/bulk-upload', 'class'=>'form-horizontal')) !!}
			<div class="form-body">
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('dataFile'))? ' has-error':'' !!}">
							{!! Form::label('dataFile', 'CSV Data File', array('class'=>'control-label col-md-3')) !!}
							<div class="col-md-9">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group input-large">
										<div class="form-control uneditable-input span3" data-trigger="fileinput">
											<i class="fa fa-file fileinput-exists"></i>&nbsp;
											<span class="fileinput-filename"></span>
										</div>
										<span class="input-group-addon btn default btn-file">
											<span class="fileinput-new"> Select file </span>
											<span class="fileinput-exists"> Change </span>
											{!! Form::file('dataFile') !!}
										</span>
										<a href="#" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
										@if ($errors->has('dataFile'))
										<span class="help-block">{!! $errors->first('dataFile') !!}</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<?php
							$is_header = Input::old('is_header');
							if ($is_header == '')
								$is_header = 1;
						?>
						<div class="form-group{!! ($errors->has('is_active'))? ' has-error':'' !!}">
							{!! Form::label('is_header', 'Column Names (Headers) Included', array('class'=>'control-label col-md-3')) !!}
							<div class="col-md-9">
								<div class="radio-list">
									<label class="radio-inline">
										<input type="radio" name="is_header" value="1" <?php if ($is_header == 1) { echo 'checked="checked"'; } ?> /> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="is_header" value="0" <?php if ($is_header == 0) { echo 'checked="checked"'; } ?>/> No
									</label>
								</div>
								@if ($errors->has('is_header'))
								<span class="help-block">{!! $errors->first('is_header') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue"><i class="fa fa-upload"></i> Upload</button>
								<a href="{!! URL::to('tag') !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
		<div class="well">
			<h4 class="block">Note</h4>
			<p>
				<ul>
					<li>The maximum file size for uploads is <strong>10 MB</strong>.</li>
					<li>Only CSV files (<strong>Comma Separated</strong>) are allowed.</li>
					<li><a href="{!! URL::to('download?name=' . urlencode('tag.csv') . '&file=' . urlencode('/data-files/downloads/samples/tag.csv')) !!}">Click Here</a> to download a Sample Tag CSV file.</li>
					<li><a href="javascript:;" id="dataFileExplanation">Click Here</a> for explanation of Sample Tag CSV file.</li>
				</ul>
			</p>
		</div>
	</div>
</div>
<!-- END :: Form -->
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
	{!! HTML::script('assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/bootbox/bootbox.min.js') !!}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/label/ui-alert-dialog-api.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			UIAlertDialogApi.init();
		});
	</script>
@stop
