@extends('admin.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
<?php
	$state = $page->getBody()->getDataByKey('state')->toArray();
?>
<!-- START :: Form -->
<div class="row">
	<div class="col-md-12">
		<div class="tabbable tabbable-custom boxless tabbable-reversed">
			@include('admin.elements.nav')
			<div class="tab-content">
				<div class="tab-pane active" id="tab_0">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-database"></i>{!! $state['name'] !!} Details
							</div>
						</div>
						<div class="portlet-body form">
							<div class="form-horizontal" role="form">
								<div class="form-body">
									<!-- START :: row -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Name:</label>
												<div class="col-md-9">
													<p class="form-control-static">
														{!! $state['name'] !!}
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Country:</label>
												<div class="col-md-9">
													<p class="form-control-static">
														<a href="{!! URL::to('country/' . $state['country_id']) !!}">{!! $state['country']['name'] !!}</a>
													</p>
												</div>
											</div>
										</div>
									</div>
									<!-- END :: row -->
									<!-- START :: row -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Status:</label>
												<div class="col-md-9">
													<p class="form-control-static">
													@if ($state['is_active'] == 1)
														Active
													@else
														In-active
													@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<!-- END :: row -->
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<a href="{!! URL::to('state') !!}" class="btn blue-madison"><i class="fa fa-list"></i> Listing</a>
													<a href="{!! URL::to('state/' . $state['id'] . '/edit') !!}" class="btn green"><i class="fa fa-pencil"></i> Edit</a>
													{!! Form::open(array('method'=>'DELETE', 'url'=>'/state', 'style'=>'display:inline', 'id'=>'DeletForm')) !!}
														<a title="Delete" id="{!! $state['id'] !!}" class="confirmDelete btn red"><i class="fa fa-trash-o"></i> Delete</a>
													{!! Form::close() !!}
													<a href="{!! URL::previous() !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
												</div>
											</div>
										</div>
										<div class="col-md-6"></div>
									</div>
								</div>
							</div>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
	{!! HTML::script('assets/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/bootbox/bootbox.min.js') !!}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/general/ui-alert-dialog-api.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			UIAlertDialogApi.init();
		});
	</script>
@stop
