@extends('admin.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
	{!! HTML::style('assets/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
	<?php
		$items = $page->getBody()->getDataByKey('list')->getCollection()->toArray();
		$country = ($page->getBody()->getDataByKey('country'))? $page->getBody()->getDataByKey('country')->toArray() : null;
	?>
	<script>
		var id = <?php echo ($country)? $country['id'] : 0; ?>; // if dependancy route then stores parent id
	</script>
	@if($country)
	<div class="row">
		<div class="col-md-12">
			<div class="tabbable tabbable-custom boxless tabbable-reversed">
				@include('admin.elements.nav')
				<div class="tab-content">
					<div class="tab-pane active" id="tab_0">
	@endif
						<!-- START :: Managed Table -->
						<div class="portlet box blue-madison">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i>{!! ($country)? $country['name'] : '' !!} State List
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-toolbar">
									<div class="row">
										<div class="col-md-6">
											<div class="btn-group">
												<a href="{!! URL::to('state/create' . (($country)? '?country_id=' . $country['id'] : '')) !!}" alt="Add">
													<button id="btn_add_new" class="btn green">Add New <i class="fa fa-plus"></i></button>
												</a>
											</div>
										</div>
										<div class="col-md-6">
											<div class="btn-group pull-right">
											</div>
										</div>
									</div>
								</div>
								@if($items)
								<table class="table table-striped table-bordered table-hover" id="managed_table_1">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Name</th>
											<th>Country</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
									@foreach ($items as $state)
										<tr class="odd gradeX">
											<td>
											@if($state['is_active'])
												<span class="label bg-green">Active</span>
											@else
												<span class="label bg-grey">Inactive</span>
											@endif
											</td>
											<td>{{ $state['name'] }}</td>
											<td>{{ $state['country']['name'] }}</td>
											<td>
												<a href="{!! URL::to('state/' . $state['id'] . '/edit') !!}" title="Edit"><span class='btn btn-xs yellow icon-pencil'> Edit</span></a>
												<a href="{!! URL::to('state/' . $state['id']) !!}" title="Details"><span class='btn btn-xs blue icon-info'> Details</span></a>
												{!! Form::open(array('method'=>'DELETE', 'url'=>URL::full(), 'style'=>'display:inline', 'id'=>'DeletForm')) !!}
													<a title="Delete" id="{!! $state['id'] !!}" class="confirmDelete btn btn-xs red icon-trash"> Delete</a>
												{!! Form::close() !!}
											</td>
										</tr>
									@endforeach
								</tbody>
								</table>
								@else
								<div class="alert alert-block alert-warning fade in">
									<button type="button" class="close" data-dismiss="alert"></button>
									<p>{!! Lang::get('No results') !!}</p>
								</div>
								@endif
							</div>
						</div>
						<!-- END :: Managed Table -->
	@if($country)
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
	{!! HTML::script('assets/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/bootbox/bootbox.min.js') !!}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/state/table-managed.js') !!}
	{!! HTML::script('assets/scripts/general/ui-alert-dialog-api.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			TableManaged.init(id);
		});
		$('body').on('mouseover','.confirmDelete',function(){
			UIAlertDialogApi.init();
		}).on('mouseout','.confirmDelete',function(){
			$('.confirmDelete').unbind('click');
		});
	</script>
@stop
