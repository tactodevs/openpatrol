@include('admin/elements/headTwoColumn')
@include('admin/elements/header')
	<!-- START :: Container -->
	<div class="page-container">
		@include('admin/elements/sidebar')
		<!-- START :: Page -->
		<div class="page-content-wrapper">
			<!-- START :: Page Container -->
			<div class="page-content">
				<!-- START :: Body Content Header -->
				<!-- START :: Notices -->
				@include('admin/elements/notices')
				<!-- END :: Notices -->
				<!-- START :: Page Title & Breadcrumb -->
				@if($page->getBody()->getTitle())
				<h3 class="page-title">{!!$page->getBody()->getTitle()!!}</h3>
				@endif
				@include('admin/elements/breadcrumbs')
				<!-- END :: Page Title & Breadcrumb -->
				<!-- END :: Body Content Header -->
				<!-- START :: Body Content -->
				<div class="row">
					<div class="col-md-12">
						@yield('bodyContent')
					</div>
				</div>
				<!-- END :: Body Content -->
			</div>
			<!-- END :: Page Container -->
		</div>
		<!-- END :: Page -->
	</div>
	<!-- END :: Container -->
@include('admin/elements/footer')