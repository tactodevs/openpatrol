@extends('admin.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
<?php
	$user = $page->getBody()->getDataByKey('user');
?>
<!-- START :: Form -->
<div class="row">
	<div class="col-md-12">
		<div class="tabbable tabbable-custom boxless tabbable-reversed">
			@include('admin.elements.nav')
			<div class="tab-content">
				<div class="tab-pane active" id="tab_0">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-database"></i>{!! $user['firstname'] !!} Details
							</div>
						</div>
						<div class="portlet-body form">
							<div class="form-horizontal" role="form">
								<div class="form-body">
									<!-- START :: row -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Name:</label>
												<div class="col-md-9">
													<p class="form-control-static">
														{!! $user['firstname'].' '.$user['lastname'] !!}
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Email:</label>
												<div class="col-md-9">
													<p class="form-control-static">
														{!! $user['email'] !!}
													</p>
												</div>
											</div>
										</div>
									</div>
									<!-- END :: row -->	
									<!-- START :: row -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Dob:</label>
												<div class="col-md-9">
													<p class="form-control-static">
														{!! $user['dob'] !!}
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Mobile:</label>
												<div class="col-md-9">
													<p class="form-control-static">
														{!! $user['mobile'] !!}
													</p>
												</div>
											</div>
										</div>
									</div>
									<!-- END :: row -->	
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Status:</label>
												<div class="col-md-9">
													<p class="form-control-static">
													@if ($user['is_active'] == 1)
														Active
													@else
														In-active
													@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<!-- END :: row -->
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<a href="{!! URL::to('user/' . $user['id'] . '/edit') !!}" class="btn green"><i class="fa fa-pencil"></i> Edit</a>
													{!! Form::open(array('method'=>'DELETE', 'url'=>'label', 'style'=>'display:inline', 'id'=>'DeletForm')) !!}
														<a title="Delete" id="{!! $user['id'] !!}" class="confirmDelete btn red"><i class="fa fa-trash-o"></i> Delete</a>
													{!! Form::close() !!}
													<a href="{!! URL::previous() !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
												</div>
											</div>
										</div>
										<div class="col-md-6"></div>
									</div>
								</div>
							</div>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
	{!! HTML::script('assets/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
	{!! HTML::script('assets/metronic/global/plugins/bootbox/bootbox.min.js') !!}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/general/ui-alert-dialog-api.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			UIAlertDialogApi.init();
		});
	</script>
@stop
