@extends('admin.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
{!! HTML::style('assets/metronic/global/plugins/select2/select2.css') !!}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
<?php
	$city = $page->getBody()->getDataByKey('city');
?>
<!-- START :: Form -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Create Law
		</div>
	</div>
	<div class="portlet-body form">
		{!! Form::open(array('name'=>'create', 'method'=>'POST', 'url'=>'law', 'class'=>'form-horizontal')) !!}
			<div class="form-body">
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('firstname'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								First Name
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('firstname', Input::old('firstname'), array('class'=>'form-control')) !!}
								@if ($errors->has('firstname'))
								<span class="help-block">{!! $errors->first('firstname') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('lastname'))? ' has-error':'' !!}">
							<label for="lastname" class="control-label col-md-3">
								Last Name
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('lastname', Input::old('lastname'), array('class'=>'form-control')) !!}
								@if ($errors->has('lastname'))
								<span class="help-block">{!! $errors->first('lastname') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('email'))? ' has-error':'' !!}">
							<label for="email" class="control-label col-md-3">
								Email
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('email', Input::old('email'), array('class'=>'form-control')) !!}
								@if ($errors->has('email'))
								<span class="help-block">{!! $errors->first('email') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('mobile'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Mobile
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('mobile', Input::old('mobile'), array('class'=>'form-control')) !!}
								@if ($errors->has('mobile'))
								<span class="help-block">{!! $errors->first('mobile') !!}</span>
								@endif
							</div>
							
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('facebook_id'))? ' has-error':'' !!}">
							<label for="facebook_id" class="control-label col-md-3">
								Facebook Id
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('facebook_id', Input::old('facebook_id'), array('class'=>'form-control')) !!}
								@if ($errors->has('facebook_id'))
								<span class="help-block">{!! $errors->first('facebook_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('google_id'))? ' has-error':'' !!}">
							<label for="google_id" class="control-label col-md-3">
								Google Id
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('google_id', Input::old('google_id'), array('class'=>'form-control')) !!}
								@if ($errors->has('google_id'))
								<span class="help-block">{!! $errors->first('google_id') !!}</span>
								@endif
							</div>
							
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
				
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('yahoo_id'))? ' has-error':'' !!}">
							<label for="yahoo_id" class="control-label col-md-3">
								Yahoo Id
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('yahoo_id', Input::old('yahoo_id'), array('class'=>'form-control')) !!}
								@if ($errors->has('yahoo_id'))
								<span class="help-block">{!! $errors->first('yahoo_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<?php
							$is_active = Input::old('is_active');
							if ($is_active == '')
								$is_active = 1;
						?>
						<div class="form-group{!! ($errors->has('is_active'))? ' has-error':'' !!}">
							{!! Form::label('is_active', 'Active', array('class'=>'control-label col-md-3')) !!}
							<div class="col-md-9">
								<div class="radio-list">
									<label class="radio-inline">
										<input type="radio" name="is_active" value="1" <?php if ($is_active == 1) { echo 'checked="checked"'; } ?> /> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="is_active" value="0" <?php if ($is_active == 0) { echo 'checked="checked"'; } ?>/> No
									</label>
								</div>
								@if ($errors->has('is_active'))
								<span class="help-block">{!! $errors->first('is_active') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue"><i class="fa fa-save"></i> Save</button>
								<a href="{!! URL::previous() !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<!-- END :: Form -->
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
{!! HTML::script('assets/metronic/global/plugins/select2/select2.min.js') !!}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/law/components-dropdowns.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			ComponentsDropdowns.init();
		});
	</script>
@stop
