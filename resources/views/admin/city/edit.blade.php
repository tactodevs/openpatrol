@extends('admin.layout.twoColumn')

@section('pageHeadSpecificPluginCSS') {{-- Page Head Specific Plugin CSS Files --}}
@stop

@section('pageHeadSpecificCSS')	{{-- Page Head Specific CSS Files --}}
	{!! HTML::style('assets/metronic/global/plugins/select2/select2.css') !!}
@stop

@section('bodyContent')	{{-- Page Body Content --}}
<?php
	$city = $page->getBody()->getDataByKey('city');
	$state = $page->getBody()->getDataByKey('state');
?>
<!-- START :: Form -->
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-edit"></i>Edit City: {!! $city['name'] !!}
		</div>
	</div>
	<div class="portlet-body form">
		{!! Form::model($city, array('name'=>'edit', 'method'=>'PUT', 'url'=> 'city/' . $city['id'], 'class'=>'form-horizontal')) !!}
			<div class="form-body">
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('name'))? ' has-error':'' !!}">
							<label for="name" class="control-label col-md-3">
								Name
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								{!! Form::text('name', Input::old('name'), array('class'=>'form-control')) !!}
								@if ($errors->has('name'))
								<span class="help-block">{!! $errors->first('name') !!}</span>
								@endif
							</div>
						</div>
					</div>
					<?php
						$stateName = ($state)? $state['name'] : $city['state']['name'];
					?>
					<div class="col-md-6">
						<div class="form-group{!! ($errors->has('state_id'))? ' has-error':'' !!}">
							<label for="state_id" class="control-label col-md-3">
								State
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<div class="select2-container form-control select2" id="s2id_select2_sample6">
									{!! Form::hidden('state_id', Input::old('state_id'), array('id'=>'select2_state_id', 'data-text'=>$stateName, 'class'=>'form-control select2')) !!}
								</div>
								@if ($errors->has('state_id'))
								<span class="help-block">{!! $errors->first('state_id') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
				<!-- START :: row -->
				<div class="row">
					<div class="col-md-6">
						<?php
							$is_active = Input::old('is_active');
							if ($is_active == '')
								$is_active = $city['is_active'];
						?>
						<div class="form-group{!! ($errors->has('is_active'))? ' has-error':'' !!}">
							{!! Form::label('is_active', 'Active', array('class'=>'control-label col-md-3')) !!}
							<div class="col-md-9">
								<div class="radio-list">
									<label class="radio-inline">
										<input type="radio" name="is_active" value="1" <?php if ($is_active == 1) { echo 'checked="checked"'; } ?> /> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="is_active" value="0" <?php if ($is_active == 0) { echo 'checked="checked"'; } ?>/> No
									</label>
								</div>
								@if ($errors->has('is_active'))
								<span class="help-block">{!! $errors->first('is_active') !!}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END :: row -->
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue"><i class="fa fa-save"></i> Save</button>
								<a href="{!! URL::previous() !!}" class="btn default"><i class="fa fa-undo"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<!-- END :: Form -->
@stop

@section('pageFooterSpecificPlugin')	{{-- Page Footer Specific Plugin Files --}}
	{!! HTML::script('assets/metronic/global/plugins/select2/select2.min.js') !!}
@stop

@section('pageFooterSpecificJS')	{{-- Page Footer Specific JS Files --}}
	{!! HTML::script('assets/metronic/global/scripts/metronic.js') !!}
	{!! HTML::script('assets/metronic/admin/layout/scripts/layout.js') !!}
	{!! HTML::script('assets/scripts/city/components-dropdowns.js') !!}
@stop

@section('pageFooterScriptInitialize')	{{-- Page Footer Script Initialization Code --}}
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			ComponentsDropdowns.init();
		});
	</script>
@stop
