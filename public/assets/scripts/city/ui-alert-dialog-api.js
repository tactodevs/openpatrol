var UIAlertDialogApi = function () {
	var handleDialogs = function() {
		$('#dataFileExplanation').click(function() {
			var alertText = '<strong>The City CSV file</strong><br />' +
				'CSV is the only accepted form for importing cities via bulk upload.<br />' +
				'You can use the Sample City CSV file as a template for creating the required CSV file. Just remember to remove the example store.<br /><br />' +
				'<em>Step 1: City CSV File Format</em><br />' +
				'The first line of your City CSV must include the field headers (described below), in the same order - each separated by a comma. Subsequent lines in the file must contain data corresponding to the headers.<br /><br />' +
				'Description of headers:' +
				'<ul>' +
				'	<li>' +
				'		<span style="text-decoration:underline;">Name</span><br />' +
				'		This field should contain the name of the city. Rules:' +
				'		<ul>' +
				'			<li>Required field</li>' +	
				'			<li>It cannot exceed 100 characters</li>' +
				'			<li>Allowed characters for name are a-z (lowercase and capital), brackets \'(\' and \')\', single quote (\'), dot (.) and space ( )' +
				'		</ul>' +
				'	</li>' +
				'	<li>' +
				'		<span style="text-decoration:underline;">City Code</span><br />' +
				'		This field should contain the code of the corresponding city. Rules:' +
				'		<ul>' +
				'			<li>It cannot exceed 6 characters</li>' +
				'		</ul>' +
				'	</li>' +
				'	<li>' +
				'		<span style="text-decoration:underline;">State ID</span><br />' +
				'		This field should contain the name of the corresponding state. Rules:' +
				'		<ul>' +
				'			<li>Required field</li>' +
				'			<li>ID of the corresponding entry in the State table</li>' +
				'		</ul>' +
				'	</li>' +
				'	<li>' +
				'		<span style="text-decoration:underline;">Latitude</span><br />' +
				'		This field should contain the corresponding latitude. Rules:' +
				'		<ul>' +
				'			<li>Must be numeric</li>' +
				'		</ul>' +
				'	</li>' +
				'	<li>' +
				'		<span style="text-decoration:underline;">Longitude</span><br />' +
				'		This field should contain the corresponding longitude. Rules:' +
				'		<ul>' +
				'			<li>Must be numeric</li>' +
				'		</ul>' +
				'	</li>' +
				'	<li>' +
				'		<span style="text-decoration:underline;">Alias</span><br />' +
				'		This field should contain the alias (alternative name) of the city. Rules:' +
				'		<ul>' +
				'			<li>It cannot exceed 200 characters</li>' +
				'			<li>Allowed characters for alias are a-z (lowercase and capital), numbers (0-9), brackets \'(\' and \')\', single quote (\'), dot (.) and space ( )</li>' +
				'		</ul>' +
				'	</li>' +
				'	<li>' +
				'		<span style="text-decoration:underline;">Active</span><br />' +
				'		This field should is a flag for whether the entry is active or not. If column value is absent, the default value is active. Rules:' +
				'		<ul>' +
				'			<li>Must 1 (active) or 0 (in-active)</li>' +
				'		</ul>' +
				'	</li>' +
				'</ul>' +
				'<em>Step 2: Create your City CSV file</em><br />' +
				'Once you\'ve populated your CV file with valid data, save the file in <a href="http://en.wikipedia.org/wiki/UTF-8">UTF-8</a> format using LF-style linefeeds. If you are not familiar with encodings, please see your spreadsheet or text editor program\'s documentation.<br /><br />' +
				'Now you can upload your CSV file for bulk-insert';
			
			bootbox.alert(alertText);
		});
	}
	
    return {
		//main function to initiate the module
		init: function () {
			handleDialogs();
		}
	};
}();