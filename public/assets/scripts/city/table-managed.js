var TableManaged = function (i) {
	var initTable = function (i) {
		var table = $('#managed_table_1');
		if(!parseInt(i)){
			i=0;
		}
		// begin first table
		table.dataTable({
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": "/city_processing?state_id="+i,
			"aaSorting": [[ 3, "desc" ]],
			"columns": [ { data: null , render: function ( data ) 
                    {
                     return data['0'] == 1 ? '<span class="label bg-green">Active</span>' : '<span class="label bg-grey">Inactive</span>'; 
                    }  
                  },{data:null,render: function(data){
						  return '  <a href="/city/'+data['6']+'" title="Details">'+data[1]+'</a>';
				  }},{data:null,render: function(data){
						  return '  <a href="/area/'+data['5']+'" title="Details">'+data[2]+'</a>';
				  }},{data:null,render: function(data){
						  return data['3'];
				  }},{data:null,render: function(data){
						  return data['4'];
				  }},{data:null,render: function(data){
						var str='<a onclick="" title="Delete" id="'+data['6']+'" class="confirmDelete btn btn-xs red icon-trash"> Delete</a>';					  
						  return '<a href="/city/'+data['6']+'/edit" title="Edit"><span class="btn btn-xs yellow icon-pencil"> Edit</span></a> \n\
						  <a href="/city/'+data['6']+'" title="Details"><span class="btn btn-xs blue icon-info"> Details</span></a>\n\
					'+str;
				  }}],	
			"lengthMenu": [
				[10, 25, 50, 75, 100, -1],
				[10, 25, 50, 75, 100, "All"] // change per page values here
			],
			"fnDrawCallback":function(oSettings){
				var total=oSettings.fnRecordsTotal();
				$('#managed_table_1_length label select').hover(function(){
				$('#managed_table_1_length label select option:eq(5)').val(total);   
				}); 		    
			},
			"iDisplayLength":25,
			// set the initial value
			"info": true,	// hidding the default info text 'Showing N to M of L entries
			"ordering": true,	// hidding the ordering arrows
			"paging": true,	// hidding the default paging buttons i.e. first, previous, page numbers, next, last
			"searching": true,	// hidding the default search box
			"columnDefs": [{  // set default column settings
				'orderable': true,
				'targets': [0,1,2]
			}, {
				"searchable": true,
				"targets": [0,3]
			}],
			"order": [
				[1, "asc"]
			] // set first column as a default sort by asc
		});
	}
	
	return {
		//main function to initiate the module
		init: function (i) {
			if (!jQuery().dataTable) {
				return;
			}
			
			initTable(i);
		}
	};
}();