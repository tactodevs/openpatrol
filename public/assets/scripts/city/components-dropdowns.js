var ComponentsDropdowns = function () {
	var handleSelect2 = function () {
		function stateFormatResult(data) {
			return data.name;
		}
		
		function stateFormatSelection(data) {
			$('#select2_state_id').val(data.id);
			
			return data.name;
		}
		
		$("#select2_state_id").select2({
			placeholder: "Select a State",
			allowClear:true,
			minimumInputLength: 3,
			ajax: {
				url: "/api/v1/state",
				dataType: 'json',
				data: function (term) {
                    return {
                        search: term, // search term
						active: 1,
                        fields: 'id,name'
                    };
                },
				results: function (response) { // parse the results into the format expected by Select2.
					return {
						results: response.data
					};
				}
			},
			
			initSelection: function (element, callback) {
				var data = {id: $(element).val(), active: 1, name: $(element).attr('data-text')};
				callback(data);
			},
			
			formatResult: stateFormatResult,
			formatSelection: stateFormatSelection,
			dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller
		});
	}
	
	return {
		// main function to initiate the module
		init: function () {
			handleSelect2();
		}
	};
}();