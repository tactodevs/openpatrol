var TableManaged = function () {
	var initTable = function () {
		var table = $('#managed_table_1');
		// begin first table
		table.dataTable({
			"columns": [
				{ "orderable": false },
				{ "orderable": false },
				{ "orderable": false },
				{ "orderable": false }
			],
			"lengthMenu": [
				[25, 50, 100, -1],
				[25, 50, 100, "All"] // change per page values here
			],
			// set the initial value
			"info": false,	// hidding the default info text 'Showing N to M of L entries
			"ordering": false,	// hidding the ordering arrows
			"paging": false,	// hidding the default paging buttons i.e. first, previous, page numbers, next, last
			"searching": false,	// hidding the default search box
			"columnDefs": [{  // set default column settings
				'orderable': false,
				'targets': [0,1,2,3]
			}, {
				"searchable": false,
				"targets": [0,3]
			}],
			"order": [
				[1, "asc"]
			] // set first column as a default sort by asc
		});
	}
	
	return {
		//main function to initiate the module
		init: function () {
			if (!jQuery().dataTable) {
				return;
			}
			
			initTable();
		}
	};
}();