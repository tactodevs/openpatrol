var ComponentsDropdowns = function () {
	var handleSelect2 = function () {
		function cityFormatResult(data) {
			return data.name;
		}
		
		function cityFormatSelection(data) {
			$('#select2_city_id').val(data.id);
			
			return data.name;
		}
		
		$("#select2_city_id").select2({
			placeholder: "Select a City",
			allowClear: true,
			minimumInputLength: 3,
			ajax: {
				url: "/api/v1/city",
				dataType: 'json',
				data: function (term) {
                    return {
                        search: term, // search term
						active: 1,
                        fields: 'id,name,state_id'
                    };
                },
				results: function (response) { // parse the results into the format expected by Select2.
					var data = response.data
					if (data.length > 0) {
						for (var i=0; i<data.length; i++)
							data[i].name = data[i].name + ', ' + data[i].state.name;
					}
					return {
						results: response.data
					};
				}
			},
			
			initSelection: function (element, callback) {
				var data = {id: $(element).val(), active: 1, name: $(element).attr('data-text')};
				callback(data);
			},
			
			formatResult: cityFormatResult,
			formatSelection: cityFormatSelection,
			dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller
		});
		
		function categoryFormatResult(data) {
			return data.name;
		}
		
		function categoryFormatSelection(data) {
			$('#select2_category_id').val(data.id);
			return data.name;
		}
		
		$("#select2_category_id").select2({
			placeholder: "Select a Category",
			allowClear: true,
			minimumInputLength: 3,
			ajax: {
				url: "/api/v1/category",
				dataType: 'json',
				data: function (term) {
                    return {
                        search: term, // search term
						active: 1,
                        fields: 'id,name'
                    };
                },
				results: function (response) { // parse the results into the format expected by Select2.
					return {
						results: response.data
					};
				}
			},
			
			initSelection: function (element, callback) {
				var data = {id: $(element).val(), active: 1, name: $(element).attr('data-text')};
				callback(data);
			},
			
			formatResult: categoryFormatResult,
			formatSelection: categoryFormatSelection,
			dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller
		});
		
	}
	
	return {
		// main function to initiate the module
		init: function () {
			handleSelect2();
		}
	};
}();